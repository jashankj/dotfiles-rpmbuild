%global	_follow_head	0
%if %{_follow_head} == 1
%global	forgeurl	https://github.com/emacs-mirror/emacs
%global	commit		c6bfffa9fe1af7f4f806e5533ba5f3c33476cf9a
%global	_v_major	30
%forgemeta
%else
%global	_v_major	29
%global	_v_minor	4
%endif
%global	_release	5
# In order to replace the Fedora `emacs' package set,
# we must pretend to also be epoch 1.
%global	_epoch		1

%bcond_without	mailutils
%bcond_with	pop
%bcond_with	kerberos
%bcond_with	kerberos5
%bcond_with	hesiod
%bcond_with	mail_unlink

%bcond_without	sound_alsa
%bcond_with	sound_oss

%bcond_with	x_toolkit_gtk2
%bcond_with	x_toolkit_gtk3
%bcond_with	x_toolkit_lucid
%bcond_without	x_toolkit_athena

%bcond_without	wide_int
%bcond_without	xpm
%bcond_without	jpeg
%bcond_without	tiff
%bcond_without	gif
%bcond_without	png
%bcond_without	rsvg
%bcond_without	webp
%bcond_without	sqlite3
%bcond_without	lcms2
%bcond_with	libsystemd
%bcond_without	cairo
%bcond_without	xml2
%bcond_with	imagemagick
%bcond_without	json
%bcond_without	tree_sitter
%bcond_without	xft
%bcond_without	harfbuzz
%bcond_without	libotf
%bcond_without	m17n_flt
%bcond_without	toolkit_scroll_bars
%bcond_without	xaw3d
%bcond_without	xim
%bcond_without	xdbe
%bcond_with	ns
%bcond_with	pgtk
%bcond_with	gpm
%bcond_without	dbus
%bcond_with	gconf
%bcond_without	gsettings
%bcond_with	selinux
%bcond_without	gnutls
%bcond_without	zlib
%bcond_without	modules
%bcond_without	threads
%bcond_without	xinput2
%bcond_with	compress_install
%bcond_without	native_comp
%bcond_without	x
%bcond_without	libgmp
%bcond_with	included_regex

#global	_fedora_default_mono_font	dejavu-sans-mono-fonts
%global	_fedora_default_mono_font	google-noto-sans-mono-vf-fonts

Name:		emacs-jashankj

Epoch:		%{_epoch}
%if %{_follow_head} == 1
Version:	%{_v_major}.0.50
%else
Version:	%{_v_major}.%{_v_minor}
%endif
Release:	%{_release}%{?dist}

Summary:	GNU Emacs text editor (with jashankj@ patches)

# The entire source code is GPLv3+ except lib-src/etags.c which is
# also BSD.  Manual (info) is GFDL.
License:	GPL-3.0-or-later AND CC0-1.0 AND GFDL-3.0-or-later AND BSD-2-Clause
URL:		https://www.gnu.org/software/emacs/

%if %{_follow_head} == 1
Source0:	%{forgesource}
#Source0:	https://github.com/emacs-mirror/emacs/archive/${commit}/emacs-${shortcommit}.tar.gz
#Source0:	https://emba.gnu.org/emacs/emacs/-/archive/${commit}/emacs-${shortcommit}.tar.gz
%else
Source0:	https://ftpmirror.gnu.org/emacs/emacs-%{version}.tar.xz
Source1:	https://ftpmirror.gnu.org/emacs/emacs-%{version}.tar.xz.sig
# Emacs 29+ signing key (eliz)
Source2:	https://keys.openpgp.org/vks/v1/by-fingerprint/17E90D521672C04631B1183EE78DAE0F3115E06B
# Emacs 29+ signing key (kangas)
Source3:	https://keys.openpgp.org/vks/v1/by-fingerprint/CEA1DE21AB108493CC9C65742E82323B8F4353EE
%endif

# Source3:	https://git.savannah.gnu.org/gitweb/?p=gnulib.git;a=blob_plain;f=lib/cdefs.h;hb=refs/heads/master#./cdefs.h

Source4:	dotemacs.el
Source5:	site-start.el
Source6:	default.el

### Distro-specific patches:
# rhbz#713600
Patch001:		001-emacs-spellchecker.patch
# rhbz#1179285: Utilize system-wide crypto-policies
Patch002:		002-emacs-system-crypto-policies.patch

# Introduce a dependency on pkgconfig(systemd).  Broken.
#XXX#Patch003:		003-emacs-libdir-vs-systemd.patch
# pgtk-on-x11 fixes; unnecessary
#XXX#Patch004:		004-emacs-desktop.patch
#XXX#Patch005:		005-emacs-pgtk-on-x-error-message.patch

### Upstream distro-specific patches to drop soon:
# (upstream as e81f1faca4382ed5c8f15fec84fb7c900a5468f9)
#Patch104:		104-emacs-pdmp-fingerprint.patch
# (upstream as 6c1413d5ef0d1fea639b0d8c83a0c0065d99359b)
#Patch105:		105-emacs-configure-c99-1.patch
# (upstream as 121a9ff9f6fc69066ce30c2dbe6cbfbfdca6aeaa)
#Patch106:		106-emacs-configure-c99-2.patch
# CVE-2022-45939 (upstream d48bb4874bc6cd3e69c7a15fc3c91cc141025c51#.)
#Patch107:		107-fixed-ctags-local-command-execute-vulnerability.patch
# debbugs#60208; backport of e59216d3be86918b995bd63273c851ebc6176a83
#Patch108:		108-native-compile-with_-Q.patch
# (upstream as 7287b7b53a17f9b2d1b474466106806a9d57af47)
#Patch109:		109-webkit2gtk-4.1.patch
# (upstream as 6c1413d5ef0d1fea639b0d8c83a0c0065d99359b)
#Patch110:		110-emacs-configure-i486-linuxaout.patch

#### Mine:
Patch500:		500-libprofiler-v6.patch
Patch510:		510-clang-quiesce.patch
Patch520:		520-outline-refcard.patch

########################################################################

#### Provide a Fedora-compatible Emacs distribution.

Requires:	emacs-filesystem
Provides:	emacs(bin)		= %{epoch}:%{version}-%{release}
Provides:	emacs-common		= %{epoch}:%{version}-%{release}
Provides:	emacs-devel		= %{epoch}:%{version}-%{release}
Provides:	emacs-el		= %{epoch}:%{version}-%{release}
Provides:	emacsclient		= %{epoch}:%{version}-%{release}
Conflicts:	emacs-common, emacs-devel, emacs-el, emacsclient

Supplements:	xorg-x11-server-Xorg

########################################################################

#### Requirements

### Build infrastructure:
BuildRequires:	make
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	libtool

### My preferred compilation setup:
BuildRequires:	clang, lld
%global	toolchain	clang
%global	__addr2line	llvm-addr2line
%global	__ar		llvm-ar
%global	__as		llvm-as
%global	__cxxfilt	llvm-c++filt
%global	__dwp		llvm-dwp
%global	__nm		llvm-nm
%global	__objcopy	llvm-objcopy
%global	__objdump	llvm-objdump
%global	__ranlib	llvm-ranlib
%global	__size		llvm-size
%global	__strip		llvm-strip
%global	__windres	llvm-windres

%global	__march		znver2
%global	__mtune		znver2

%global	_cflags_tune	-mtune=%{__march} -march=%{__mtune}
%global	_cflags_hush	-Wno-unknown-warning-option
%global	_cflags_builtins	-fno-builtin-malloc -fno-builtin-calloc -fno-builtin-realloc -fno-builtin-free -fno-builtin-strdup -fno-builtin-strndup -fno-builtin-aligned_alloc -fno-builtin-memalign
%global	_distro_extra_cflags	%{_cflags_tune} %{_cflags_hush} %{_cflags_builtins}

%global	_ldflags_lld	-fuse-ld=lld -Wl,--gc-sections -Wl,--threads,%{_smp_build_ncpus}
%global	_ldflags_libs	-lprofiler -ljemalloc
%global	_distro_extra_ldflags	%{_ldflags_lld} %{_ldflags_libs}

### My patches introduce dependencies:
BuildRequires:	gperftools-devel
BuildRequires:	jemalloc-devel

### Core dependencies:
BuildRequires:	glibc-devel
BuildRequires:	ncurses-devel

BuildRequires:	texinfo
Recommends:	info

BuildRequires:	desktop-file-utils
Requires:	desktop-file-utils

%if "%{_lib}" == "lib64"
%global marker ()(64bit)
%endif

# rhbz#732422: Emacs doesn't run without a font
BuildRequires:	%{_fedora_default_mono_font}
Requires:	%{_fedora_default_mono_font}

BuildRequires:	libacl-devel
BuildRequires:	pkgconfig(libseccomp) >= 2.5.2

# Needed for AppStream metadata check. (Even if we don't want AppStream?)
BuildRequires:	libappstream-glib

BuildRequires:	attr
BuildRequires:	util-linux
BuildRequires:	pax-utils

# BuildRequires:	xorg-x11-proto-devel
# BuildRequires:	atk-devel
# BuildRequires:	bzip2
# BuildRequires:	gzip

# Needed for `gpgverify' step later in the build
BuildRequires:	gnupg2

%if %{with sound_alsa}
BuildRequires:	pkgconfig(alsa) >= 1.0.0
%endif

%if %{with cairo}
# Baseline >= 1.8.0; pgtk >= 1.12.0.
BuildRequires:	pkgconfig(cairo) >= 1.12.0
BuildRequires:	pkgconfig(cairo-xlib) >= 1.8.0
%endif

%if %{with dbus}
BuildRequires:	pkgconfig(dbus-1) >= 1.0
%endif

%if %{with gif}
BuildRequires:	giflib-devel
%endif

%if %{with gnutls}
BuildRequires:	pkgconfig(gnutls) >= 2.12.2
%endif

%if %{with gpm}
BuildRequires:	gpm-devel
%endif

%if %{with gconf}
BuildRequires:	pkgconfig(gconf-2.0) >= 2.13
%endif

%if %{with gsettings}
# baseline: nil; notify GFile >= 2.24; GSettings >= 2.26
BuildRequires:	pkgconfig(gio-2.0) >= 2.26
BuildRequires:	pkgconfig(gobject-2.0) >= 2.0
%endif

%if %{with harfbuzz}
# Baseline >= 0.9.42; Win32 >= 1.2.3
BuildRequires:	pkgconfig(harfbuzz) >= 0.9.42
%endif

%if %{with imagemagick}
BuildRequires:	pkgconfig(MagickWand) >= 7
%endif

%if %{with jpeg}
BuildRequires:	pkgconfig(libjpeg)
%endif

%if %{with json}
BuildRequires:	pkgconfig(jansson) >= 2.7
%endif

%if %{with lcms2}
BuildRequires:	pkgconfig(lcms2)
%endif

%if %{with libgmp}
BuildRequires:	pkgconfig(gmp)
%endif

%if %{with libotf}
BuildRequires:	pkgconfig(libotf)
%endif

%if %{with libsystemd}
BuildRequires:	pkgconfig(libsystemd) >= 222
%endif

%if %{with m17n_flt}
BuildRequires:	pkgconfig(m17n-flt)
%endif

%if %{with native_comp}
BuildRequires:	libgccjit-devel
Requires:	libgccjit
%endif

%if %{with ns}
BuildRequires:	gnustep-base-devel
%endif

%if %{with png}
BuildRequires:	pkgconfig(libpng) >= 1.0.0
%endif

%if %{with rsvg}
BuildRequires:	pkgconfig(librsvg-2.0) >= 2.14.0
%endif

%if %{with selinux}
BuildRequires:	pkgconfig(libselinux)
%endif

%if %{with sqlite3}
BuildRequires:	pkgconfig(sqlite3)
%endif

%if %{with tree_sitter}
#BuildRequires:	pkgconfig(tree-sitter) >= 0.20.2
# rhbz#2277250, https://github.com/tree-sitter/tree-sitter/issues/3296
BuildRequires:	pkgconfig(tree-sitter) >= 0.22.5
%endif

%if %{with tiff}
BuildRequires:	pkgconfig(libtiff-4)
%endif

%if %{with webp}
BuildRequires:	pkgconfig(libwebpdemux) >= 0.6.0
%endif

%if %{with x}
BuildRequires:	pkgconfig(ice)
BuildRequires:	pkgconfig(sm)
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(xau)
BuildRequires:	pkgconfig(xdmcp)
BuildRequires:	pkgconfig(xfixes) >= 4.0.0
BuildRequires:	pkgconfig(xinerama) >= 1.0.2
BuildRequires:	pkgconfig(xrandr) >= 1.2.2
BuildRequires:	pkgconfig(xrender)
%if %{with xinput2}
BuildRequires:	pkgconfig(xi)
%endif
BuildRequires:	pkgconfig(xcb)
BuildRequires:	pkgconfig(xcb-shape)
BuildRequires:	pkgconfig(x11-xcb)

%if %{with x_toolkit_gtk2}
BuildRequires:	pkgconfig(glib-2.0) >= 2.28
BuildRequires:	pkgconfig(gtk+-2.0) >= 2.24
%endif

%if %{with x_toolkit_gtk3}
BuildRequires:	pkgconfig(glib-2.0) >= 2.37.5
# X11 GTK3 >= 3.10; PGTK GTK3 >= 3.22.23
BuildRequires:	pkgconfig(gtk+-3.0) >= 3.22.23
# rhbz#2335309
Requires:	libpixbufloader-xpm.so%{?marker}
%endif

%if %{with x_toolkit_lucid} || %{with x_toolkit_athena}
%if %{with xaw3d}
BuildRequires:	pkgconfig(xaw3d)
%else
BuildRequires:	pkgconfig(xaw7)
%endif
BuildRequires:	pkgconfig(xt)
%endif

%if %{with xdbe}
BuildRequires:	pkgconfig(xext)
%endif

%if %{with xft}
BuildRequires:	pkgconfig(fontconfig) >= 2.2.0
BuildRequires:	pkgconfig(xft) >= 0.13.0
BuildRequires:	pkgconfig(freetype2)
%endif
%endif

%if %{with xml2}
BuildRequires:	pkgconfig(libxml-2.0) > 2.6.17
%endif

%if %{with xpm}
BuildRequires:	pkgconfig(xpm)
%endif

%if %{with xwidgets}
BuildRequires:	pkgconfig(webkit2gtk4.1) >= 2.12
%endif

%if %{with zlib}
BuildRequires:	pkgconfig(zlib)
%endif

# Hm, we probably could build Emacs.app if we tried, but we don't.
#bcond_with	ns

# PGTK is known broken in my preferred config.
#bcond_with	pgtk

Recommends:	enchant2

%global	site_lisp		%{_datadir}/emacs/site-lisp
%global	site_start_d		%{site_lisp}/site-start.d
%global	pkgconfig		%{_datadir}/pkgconfig
%global	emacs_libexecdir	%{_libexecdir}/emacs/%{version}/%{_host}
%global	arch_lisp		%{_libdir}/emacs/%{version}
%global	native_lisp		%{_libdir}/emacs/%{version}/native-lisp

Requires(preun):	/usr/sbin/alternatives
Requires(posttrans):	/usr/sbin/alternatives

%description
GNU Emacs is a powerful, customizable, self-documenting, modeless text
editor. It contains special code editing features, a scripting language
(elisp), and the capability to read mail, news, and more without leaving
the editor.

This package includes patches and tweaks by jashankj@.
It can wholly supplant the default Fedora distribution of Emacs.


########################################################################

%prep
%if %{_follow_head} != 1
cat '%{SOURCE2}' '%{SOURCE3}' > keyring
%{gpgverify} \
	--keyring=keyring \
	--signature='%{SOURCE1}' \
	--data='%{SOURCE0}'
rm keyring
%endif

%if %{_follow_head} == 1
%setup	-q -n emacs-%{commit}
%else
%setup	-q -n emacs-%{version}
%endif

%patch	-P 001	-p1 -b .001
%patch	-P 002	-p1 -b .002
#patch	-P 003	-p1 -b .003
#patch	-P 004	-p1 -b .004
#patch	-P 005	-p1 -b .005

#patch	-P 104	-p1 -b .104
#patch	-P 105	-p1 -b .105
#patch	-P 106	-p1 -b .106
#patch	-P 107	-p1 -b .107
#patch	-P 108	-p1 -b .108
#patch	-P 109	-p1 -b .109
#patch	-P 110	-p1 -b .110

%patch	-P 500	    -b .500
%patch	-P 510	    -b .510
%patch	-P 520	-p1 -b .520

# Avoid duplicating doc files in the common subpackage
ln -s ../../%{name}/%{version}/etc/COPYING doc
ln -s ../../%{name}/%{version}/etc/NEWS doc

# # Avoid trademark issues
# grep -v "tetris.elc" lisp/Makefile.in > lisp/Makefile.in.new \
#    && mv lisp/Makefile.in.new lisp/Makefile.in
# rm -f lisp/play/tetris.el lisp/play/tetris.elc
# grep -v "pong.elc" lisp/Makefile.in > lisp/Makefile.in.new \
#    && mv lisp/Makefile.in.new lisp/Makefile.in
# rm -f lisp/play/pong.el lisp/play/pong.el

########################################################################

%conf

autoreconf --symlink --install --verbose --force

mkdir build && \
cd build && \
ln -sv ../configure

%configure \
	--enable-check-lisp-object-type \
	\
	%{?with_mailutils:--with-mailutils} %{!?with_mailutils:--without-mailutils} \
	%{?with_pop:--with-pop} %{!?with_pop:--without-pop} \
	%{?with_kerberos:--with-kerberos} %{!?with_kerberos:--without-kerberos} \
	%{?with_kerberos5:--with-kerberos5} %{!?with_kerberos5:--without-kerberos5} \
	%{?with_hesiod:--with-hesiod} %{!?with_hesiod:--without-hesiod} \
	%{?with_mail_unlink:--with-mail-unlink} %{!?with_mail_unlink:--without-mail-unlink} \
	\
	%{?with_sound_alsa:--with-sound=alsa} \
	%{?with_sound_oss:--with-sound=oss} \
	\
	%{?with_x_toolkit_gtk2:--with-x-toolkit=gtk2} \
	%{?with_x_toolkit_gtk3:--with-x-toolkit=gtk3} \
	%{?with_x_toolkit_lucid:--with-x-toolkit=lucid} \
	%{?with_x_toolkit_athena:--with-x-toolkit=athena} \
	\
	%{?with_wide_int:--with-wide-int} %{!?with_wide_int:--without-wide-int} \
	%{?with_xpm:--with-xpm} %{!?with_xpm:--without-xpm} \
	%{?with_jpeg:--with-jpeg} %{!?with_jpeg:--without-jpeg} \
	%{?with_tiff:--with-tiff} %{!?with_tiff:--without-tiff} \
	%{?with_gif:--with-gif} %{!?with_gif:--without-gif} \
	%{?with_png:--with-png} %{!?with_png:--without-png} \
	%{?with_rsvg:--with-rsvg} %{!?with_rsvg:--without-rsvg} \
	%{?with_webp:--with-webp} %{!?with_webp:--without-webp} \
	%{?with_sqlite3:--with-sqlite3} %{!?with_sqlite3:--without-sqlite3} \
	%{?with_lcms2:--with-lcms2} %{!?with_lcms2:--without-lcms2} \
	%{?with_libsystemd:--with-libsystemd} %{!?with_libsystemd:--without-libsystemd} \
	%{?with_cairo:--with-cairo} %{!?with_cairo:--without-cairo} \
	%{?with_xml2:--with-xml2} %{!?with_xml2:--without-xml2} \
	%{?with_imagemagick:--with-imagemagick} %{!?with_imagemagick:--without-imagemagick} \
	%{?with_json:--with-json} %{!?with_json:--without-json} \
	%{?with_tree_sitter:--with-tree-sitter} %{!?with_tree_sitter:--without-tree-sitter} \
	%{?with_xft:--with-xft} %{!?with_xft:--without-xft} \
	%{?with_harfbuzz:--with-harfbuzz} %{!?with_harfbuzz:--without-harfbuzz} \
	%{?with_libotf:--with-libotf} %{!?with_libotf:--without-libotf} \
	%{?with_m17n_flt:--with-m17n-flt} %{!?with_m17n_flt:--without-m17n-flt} \
	%{?with_toolkit_scroll_bars:--with-toolkit-scroll-bars} %{!?with_toolkit_scroll_bars:--without-toolkit-scroll-bars} \
	%{?with_xaw3d:--with-xaw3d} %{!?with_xaw3d:--without-xaw3d} \
	%{?with_xim:--with-xim} %{!?with_xim:--without-xim} \
	%{?with_xdbe:--with-xdbe} %{!?with_xdbe:--without-xdbe} \
	%{?with_ns:--with-ns} %{!?with_ns:--without-ns} \
	%{?with_pgtk:--with-pgtk} %{!?with_pgtk:--without-pgtk} \
	%{?with_gpm:--with-gpm} %{!?with_gpm:--without-gpm} \
	%{?with_dbus:--with-dbus} %{!?with_dbus:--without-dbus} \
	%{?with_gconf:--with-gconf} %{!?with_gconf:--without-gconf} \
	%{?with_gsettings:--with-gsettings} %{!?with_gsettings:--without-gsettings} \
	%{?with_selinux:--with-selinux} %{!?with_selinux:--without-selinux} \
	%{?with_gnutls:--with-gnutls} %{!?with_gnutls:--without-gnutls} \
	%{?with_zlib:--with-zlib} %{!?with_zlib:--without-zlib} \
	%{?with_modules:--with-modules} %{!?with_modules:--without-modules} \
	%{?with_threads:--with-threads} %{!?with_threads:--without-threads} \
	%{?with_xinput2:--with-xinput2} %{!?with_xinput2:--without-xinput2} \
	%{?with_small_ja_dic:--with-small-ja-dic} %{!?with_small_ja_dic:--without-small-ja-dic} \
	%{?with_xwidgets:--with-xwidgets} %{!?with_xwidgets:--without-xwidgets} \
	%{?with_compress_install:--with-compress-install} %{!?with_compress_install:--without-compress-install} \
	%{?with_native_comp:--with-native-compilation=aot} %{!?with_native_comp:--without-native-compilation} \
	%{?with_x:--with-x} %{!?with_x:--without-x} \
	%{?with_libgmp:--with-libgmp} %{!?with_libgmp:--without-libgmp} \
	%{?with_included_regex:--with-included-regex} %{!?with_included_regex:--without-included-regex}

########################################################################

%build
%set_build_flags
cd build
%make_build bootstrap
%make_build

# Remove versioned file so that we end up with .1 suffix and only one DOC file
rm src/emacs-%{version}.*

cd ..

# Create pkgconfig file
cat > emacs.pc << EOF
sitepkglispdir=%{site_lisp}
sitestartdir=%{site_start_d}

Name:		emacs
Description:	GNU Emacs text editor
Version:	%{version}
EOF

# Create macros.emacs RPM macro file
cat > macros.emacs << EOF
%%_emacs_version	%{version}
%%_emacs_ev		%{?epoch:%{epoch}:}%{version}
%%_emacs_evr		%{?epoch:%{epoch}:}%{version}-%{release}
%%_emacs_sitelispdir	%{site_lisp}
%%_emacs_sitestartdir	%{site_start_d}
%%_emacs_bytecompile(W) /usr/bin/emacs -batch --no-init-file --no-site-file --eval '(push nil load-path)' %%{-W:--eval '(setq byte-compile-error-on-warn t)' }-f batch-byte-compile %%*
EOF

########################################################################

%install
cd build
%make_install
cd ..

# Let alternatives manage the symlink
rm %{buildroot}%{_bindir}/emacs
touch %{buildroot}%{_bindir}/emacs

mkdir -p %{buildroot}%{site_lisp}
install -p -m 0644 %SOURCE5 %{buildroot}%{site_lisp}/site-start.el
install -p -m 0644 %SOURCE6 %{buildroot}%{site_lisp}

# This solves rhbz#474958, "update-directory-autoloads" now finally
# works the path is different each version, so we'll generate it here
echo "(setq source-directory \"%{_datadir}/emacs/%{version}/\")" \
	>> %{buildroot}%{site_lisp}/site-start.el

mv %{buildroot}%{_bindir}/{etags,etags.emacs}
mv %{buildroot}%{_mandir}/man1/{etags.1,etags.emacs.1}

mv %{buildroot}%{_mandir}/man1/{ctags.1,gctags.1}
mv %{buildroot}%{_bindir}/{ctags,gctags}

# Sorted list of info files
%global	info_files auth autotype bovine calc ccmode cl dbus dired-x ebrowse ede ediff edt efaq eglot eieio eintr elisp emacs-gnutls emacs-mime emacs epa erc ert eshell eudc eww flymake forms gnus htmlfontify idlwave ido mairix-el message mh-e modus-themes newsticker nxml-mode octave-mode org pcl-cvs pgg rcirc reftex remember sasl sc semantic ses sieve smtpmail speedbar srecode todo-mode tramp transient url use-package vhdl-mode vip viper vtable widget wisent woman

# rhbz#927996
mv %{buildroot}%{_infodir}/{info.info,info}
# After everything is installed, remove info dir
rm -f %{buildroot}%{_infodir}/dir

mkdir -p %{buildroot}%{site_lisp}/site-start.d

# Default initialization file
mkdir -p %{buildroot}%{_sysconfdir}/skel
install -p -m 0644 %SOURCE4 %{buildroot}%{_sysconfdir}/skel/.emacs

# Install pkgconfig file
mkdir -p %{buildroot}/%{pkgconfig}
install -p -m 0644 emacs.pc %{buildroot}/%{pkgconfig}

# Install rpm macro definition file
mkdir -p %{buildroot}%{_rpmconfigdir}/macros.d
install -p -m 0644 macros.emacs %{buildroot}%{_rpmconfigdir}/macros.d/

# Install desktop files
# desktop-file-install --dir=%{buildroot}%{_datadir}/applications %SOURCE7

# Remove duplicate desktop-related files
rm %{buildroot}%{_datadir}/emacs/%{version}/etc/emacs.{desktop,service}

# Install a systemd service.  (If only macro _userunitdir worked!)
mkdir -p %{buildroot}/usr/lib/systemd/user
mv %{buildroot}/usr/lib64/systemd/user/emacs.service \
	%{buildroot}/usr/lib/systemd/user/emacs.service

# # We don't ship the client variants yet
# # https://src.fedoraproject.org/rpms/emacs/pull-request/12
# rm %{buildroot}%{_datadir}/applications/emacsclient.desktop
# rm %{buildroot}%{_datadir}/applications/emacsclient-mail.desktop

# Remove old icon
rm %{buildroot}%{_datadir}/icons/hicolor/scalable/mimetypes/emacs-document23.svg

# Install all the pdmp with fingerprints
emacs_pdmp="emacs-$(./build/src/emacs --fingerprint 2>&1 | sed 's/.* //').pdmp"
install -p -m 0644 build/src/emacs.pdmp %{buildroot}%{emacs_libexecdir}/${emacs_pdmp}

# Install native compiled Lisp of all builds
emacs_comp_native_ver="$(ls -1 build/native-lisp)"
cp -ar build/native-lisp/${emacs_comp_native_ver} %{buildroot}%{native_lisp}

# remove exec permissions from eln files to prevent the debuginfo
# extractor from trying to extract debuginfo from them
find %{buildroot}%{_libdir}/ -name '*eln' -type f | xargs chmod -x

# ensure native files are newer than byte-code files (rhbz#2157979 #c11)
find %{buildroot}%{_libdir}/ -name '*eln' -type f | xargs touch

########################################################################
#
# Create file lists.
#
# Opting for laziness and correctness over the original's efficiency.
#

MANIFEST=${PWD}/rpm-manifest

(
	cd %{buildroot}

	find \
		.%{_datadir}/emacs/%{version}/ \
		.%{native_lisp}/${emacs_comp_native_ver} \
		-type d \
		-printf "%%%%dir	%%p\n"

	find \
		.%{_datadir}/emacs/%{version}/ \
		-type f -print

	#	\( -name '*.el' -o -name '*.el.gz' -o -name '*.elc' \)

	find \
		.%{emacs_libexecdir}/ \
		-type f \
		-name '*.pdmp' \
		-print

	find \
		.%{native_lisp}/${emacs_comp_native_ver}/ \
		-type f -name '*.eln' \
		-printf "%%%%attr(755,-,-)	%%p\n"


	for info_f in %{info_files}
	do
		echo "%{_infodir}/${info_f}.info*"
	done

	# info is a rename of info.info and thus needs special handling
	echo "%{_infodir}/info*"
) \
| sed -e "s|\.%{_prefix}|%{_prefix}|" \
| sort | uniq > $MANIFEST

########################################################################

%check
# A number of tests that don't work on GNU EMBA are also unstable when
# run in Koji.
export EMACS_EMBA_CI=1

# cd build
# %make_build check
# cd ..

appstream-util validate-relax \
	--nonet \
	%{buildroot}/%{_metainfodir}/*.metainfo.xml

desktop-file-validate \
	%{buildroot}/%{_datadir}/applications/*.desktop

%posttrans
/usr/sbin/alternatives --install \
	%{_bindir}/emacs emacs \
		%{_bindir}/emacs-%{version} 80 || :

/usr/sbin/alternatives --install \
	%{_bindir}/etags emacs.etags \
		%{_bindir}/etags.emacs 80 \
	--slave \
	%{_mandir}/man1/etags.1 emacs.etags.man \
		%{_mandir}/man1/etags.emacs.1 || :


%preun
/usr/sbin/alternatives --remove \
	emacs \
	%{_bindir}/emacs-%{version} || :

/usr/sbin/alternatives --remove \
	emacs.etags \
	%{_bindir}/etags.emacs || :

########################################################################

%files	-f rpm-manifest

%doc		README
%doc		doc/NEWS
%doc		BUGS
%license	etc/COPYING

%attr(0755,-,-)	%ghost	%{_bindir}/emacs
%{_bindir}/emacs-%{version}
%{_mandir}/man1/emacs.1*

%{_bindir}/emacsclient
%{_mandir}/man1/emacsclient.1*

%{_bindir}/ebrowse
%{_mandir}/man1/ebrowse.1*

%{_bindir}/etags.emacs
%{_mandir}/man1/etags.emacs.1*

%{_bindir}/gctags
%{_mandir}/man1/gctags.1*

%dir	%{_libdir}/emacs
%dir	%{arch_lisp}
%dir	%{native_lisp}

%dir	%{_libexecdir}/emacs
%dir	%{_libexecdir}/emacs/%{version}
%dir	%{emacs_libexecdir}
%{emacs_libexecdir}/hexl
%{emacs_libexecdir}/rcs2log

%config(noreplace)	%{_sysconfdir}/skel/.emacs

%{_rpmconfigdir}/macros.d/macros.emacs

%{_includedir}/emacs-module.h

%{_datadir}/applications/emacs.desktop
%{_datadir}/applications/emacs-mail.desktop
%{_datadir}/applications/emacsclient.desktop
%{_datadir}/applications/emacsclient-mail.desktop

%{_datadir}/icons/hicolor/*/apps/emacs.png
%{_datadir}/icons/hicolor/scalable/apps/emacs.svg
%{_datadir}/icons/hicolor/scalable/apps/emacs.ico
%{_datadir}/icons/hicolor/scalable/mimetypes/emacs-document.svg

# ${_userunitdir}/emacs.service
/usr/lib/systemd/user/emacs.service

%{_metainfodir}/emacs.metainfo.xml

%attr(0644,root,root)	%config(noreplace)	%{site_lisp}/default.el
%attr(0644,root,root)	%config			%{site_lisp}/site-start.el
%attr(0644,root,root)	%config			%{site_lisp}/subdirs.el

%{pkgconfig}/emacs.pc


########################################################################

%changelog
* Mon Feb 24 2025 Jashank Jeremy <fedora@jashankj.space> - 1:29.4-5
- Import (the effect of some) Fedora packaging fixes:
  - a64c461ff7, 563755ba21, efc6d1cc8f, 782c788973: force rebuild.

* Sun Jan 19 2025 Jashank Jeremy <fedora@jashankj.space> - 1:29.4-4
- Force rebuild.

* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 1:29.4-3
- Import (the effect of some) Fedora packaging fixes:
  - cf88b767de, 8dd6af7706, bc8ee9cedf, c8f0e7e446: rebuild
  - 2094f8176e: want pixbufloader-xpm for gtk
- Skip some Fedora packaging fixes:
  - 155673080c: not removed here
  - 78d229edc1, 05cd234a69, 3ba3bada53, f201351dff, 253c482c86:
    not present / not required
  - 12239bab40, 7510935270: we do not have multiple packages
- tests?
  - skip 87182d19c2, c39eb8eaa9, 4dac80cb55

* Mon Sep 23 2024 Jashank Jeremy <fedora@jashankj.space> - 1:29.4-2
- Import (the effect of some) Fedora packaging fixes:
  - 1d7139dcc2: relax libtreesitter requirement
- Skip some Fedora packaging fixes:
  - 07ecef2a9c: we don't -Wl,-z,relro
  - 66296574a2, 88a3c70353: xwidgets off by default

* Tue Apr 30 2024 Jashank Jeremy <fedora@jashankj.space> - 1:29.4-1
- Update to 29.4; bump for Fedora 41 mass rebuild.
- Import (the effect of some) Fedora packaging fixes:
  - 9b5c26fbad: tweak package description
  - f705000ea2: update to 29.4.
  - e3cc998c0e, dfc53dd6c8: handle updated signing key
  - d3db50bc99, 7157b76d42: run tests
  - 5e02a413da: own unowned directories
  - dec4348128: update .gitignore
  - c7d90c2305: `_hardened_build` now default
  - df45480b91: pretend to be in EMBA to skip some flaky tests
- Skip some Fedora packaging fixes:
  - 6f72315876, 5551df4c13: no split `emacsclient` (but Provide it)
  - 9bbdcd5307, 21c4c1004d: no `emacs-desktop.sh` here
  - 54190f15c1, bce17310e3, df9b814018, ec525e1cea, 21f0a822f7:
    we do not have multiple packages
  - 6ee9fc3c3b: we prefer numbered patches
  - 2860413ce2: no stellated line in this specfile
  - 9c1333df9d, 311ab8413b: no i686 cross-build support
  - 16d2054b15: we don't run pgtk
- tests?
  - c68d79e36c: test fixes
  - 290ab368cc: test fixes
  - 04dac35b639835e34e86cfcb1a6bf9aedd2f8cfc
  - 449be03f8f7114bee6e0e02f14ad7c653488ba12
  - d81023e55d67e4ecbff1c74630cfab1925c57af4
  - 88b7252ef15f1faf7478af130f94a64c25f741b9

* Tue Apr 30 2024 Jashank Jeremy <fedora@jashankj.space> - 1:29.3-8
- Bump Epoch marker to compatibly replace Fedora `emacs`.

* Sat Apr 27 2024 Jashank Jeremy <fedora@jashankj.space> - 29.3-8
- Revert use of `rpmautospec`, which is now entirely broken, yay!
- Import (the effect of some) Fedora packaging fixes:
  - 4842649133: Avoid a tree-sitter crash (rhbz#2277250).
  - 0f3de093ef: Remove liblockfile dependency.
  - 66f03e51df, 6fe2018073: Support separate emacs-filesystem.
- Skip some Fedora packaging fixes:
  - 1ebda76805: no `emacs-desktop.sh` here
  - bc7d4e0406: HTTPS URL (we already did)
  - de1de70ec0: emacs-nox fixes (not relevant)

* Sun Mar 31 2024 Jashank Jeremy <fedora@jashankj.space> - 29.3-1
- Update to 29.3.
- Import (the effect of some) Fedora packaging fixes:
  - 44811312be: update rhbz#732422 fix with default font.
  - 85031a2f73: convert to %autorelease and %autochangelog.
  - aba744d1bc: fix 'alternatives' dependency per policy.
  - 3461073c54: add -W option to %_emacs_bytecompile
- Skip some Fedora packaging fixes:
  - 44811312be, e739d345f1: we do not have multiple packages.
  - f151e68f40: we (should) be getting file lists right.
  - 9c5e49b7f0: we already had SQLite, tree-sitter, WebP, XInput2.
  - 6a817e69ea: we already express tree-sitter deps.
  - 0fd1a3e8ed: we properly declared our Xi dependency.
  - 6a39a2112e: we do not (want to) run Wayland.
  - 0d766e84f3: we want webkitgtk (dropped by RHEL 10).
  - 2778435025, 64a3d6cec1, eb9a486090: we did this already.
  - 6efc21e5fc: we properly declared our SQLite3 dependency.
  - 6a7849921b: we disable gpm by default.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 29.2-1
- Update to 29.2; bump for Fedora 40 mass rebuild.

* Sat Oct 21 2023 Jashank Jeremy <fedora@jashankj.space> - 29.1-2
- Fix broken build.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 29.1-1
- Bump for Fedora 39 mass rebuild.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 28.2-2
- Bump for Fedora 39 mass rebuild.

* Sat May 06 2023 Jashank Jeremy <fedora@jashankj.space> - 28.2-1
- Switch down to the mainline at 28.2; fix follow-mainline.

* Sat Mar 11 2023 Jashank Jeremy <fedora@jashankj.space> - 30.0.50-1.20230311gitc6bfffa
- Bump to Git c6bfffa9fe1af7f4f806e5533ba5f3c33476cf9a.
- Tear off changelog.

* Mon Feb 20 2023 Jashank Jeremy <fedora@jashankj.space> - 30.0.50-1.20230220gitcac13e3
- Bump to Git cac13e360547f95ec64d34f38003dfc7ff1a97ee.
- Use the 'forge' RPM macros.

* Sun Feb 19 2023 Jashank Jeremy <fedora@jashankj.space> - 30.0-1.20230219git750bc57
- Stand up my Emacs and patches as a RPM.
- (See https://src.fedoraproject.org/rpms/emacs for older changelog entries.)
