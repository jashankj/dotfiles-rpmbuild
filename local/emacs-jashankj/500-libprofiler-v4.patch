From ec4230901b428821d74ec82d4dff47f028332fe0 Mon Sep 17 00:00:00 2001
From: Jashank Jeremy <jashank@rulingia.com.au>
Date: Mon, 5 Apr 2021 16:48:15 +1000
Subject: [PATCH] {LOCAL} Allow the use of `libprofiler' to collect profile data.

The Emacs profiler binds the `SIGPROF' signal, which breaks `gperftools'
in a default configuration.  The easiest way to stop this happening is
apparently to undefine the symbol, which turns off the Emacs profiler.

To allow "continuous pprof"-style operation, every N garbage collection
cycles (currently N=16) we restart the profiler, which flushes out the
profile to disk.  Otherwise, we can only usefully review profiling data
after the Emacs process has gone away.
---
 src/alloc.c     | 47 +++++++++++++++++++++++++++++++++++++++++++++++
 src/callproc.c  |  4 ++++
 src/profiler.c  |  4 ++++
 src/sysdep.c    |  3 +++
 src/syssignal.h |  4 ++++
 5 files changed, 62 insertions(+)

Index: src/alloc.c
===================================================================
--- src/alloc.c.orig
+++ src/alloc.c
@@ -625,6 +625,48 @@ struct Lisp_Finalizer finalizers;
    running finalizers.  */
 struct Lisp_Finalizer doomed_finalizers;
 
+
+
+/* Ask `libprofiler' to dump profile data. */
+#include <gperftools/profiler.h>
+#include <stdio.h>
+#include <stdlib.h>
+#include <unistd.h>
+
+static bool     gperf_cpu_enabled_p = true;
+static char    *gperf_cpu_profile_dir = NULL;
+static char     gperf_cpu_file[PATH_MAX];
+static unsigned gperf_cpu_n_profiles = 0;
+
+static void
+update_profile_name (unsigned int n)
+{
+  snprintf (gperf_cpu_file, PATH_MAX,
+	    "%s/emacs.%d.profile.%u",
+	    gperf_cpu_profile_dir, getpid (), n);
+}
+
+static void
+restart_profiler (void)
+{
+  // fast-path:
+  if (! gperf_cpu_enabled_p) return;
+
+  // first-run:  enabled, dir = NULL
+  // disables if CPUPROFILE_DIR is unset
+  if (gperf_cpu_enabled_p && !gperf_cpu_profile_dir)
+    if ((gperf_cpu_profile_dir = getenv ("CPUPROFILE_DIR")) == NULL)
+      {
+	gperf_cpu_enabled_p = false;
+	return;
+      }
+
+  if (ProfilingIsEnabledForAllThreads ())
+    ProfilerStop ();
+  update_profile_name (gperf_cpu_n_profiles++);
+  ProfilerStart (gperf_cpu_file);
+}
+
 
 /************************************************************************
 				Malloc
@@ -6298,6 +6340,9 @@ garbage_collect (void)
       if (tot_after < tot_before)
 	malloc_probe (min (tot_before - tot_after, SIZE_MAX));
     }
+
+  if (gcs_done % 16 == 0)
+    restart_profiler (); /* ergh, but no easy better place */
 }
 
 DEFUN ("garbage-collect", Fgarbage_collect, Sgarbage_collect, 0, 0, "",
@@ -7747,6 +7792,8 @@ init_alloc (void)
 {
   Vgc_elapsed = make_float (0.0);
   gcs_done = 0;
+
+  restart_profiler ();
 }
 
 void
Index: src/callproc.c
===================================================================
--- src/callproc.c.orig
+++ src/callproc.c
@@ -89,6 +89,10 @@ extern char **environ;
 #include "pgtkterm.h"
 #endif
 
+#ifdef SIGPROF
+#undef SIGPROF
+#endif
+
 /* Pattern used by call-process-region to make temp files.  */
 static Lisp_Object Vtemp_file_name_pattern;
 
Index: src/profiler.c
===================================================================
--- src/profiler.c.orig
+++ src/profiler.c
@@ -23,6 +23,10 @@ along with GNU Emacs.  If not, see <http
 #include "systime.h"
 #include "pdumper.h"
 
+#ifdef SIGPROF
+#undef SIGPROF
+#endif
+
 /* Return A + B, but return the maximum fixnum if the result would overflow.
    Assume A and B are nonnegative and in fixnum range.  */
 
Index: src/sysdep.c
===================================================================
--- src/sysdep.c.orig
+++ src/sysdep.c
@@ -2055,6 +2055,9 @@ init_signals (void)
 #endif
   sigaction (SIGTERM, &process_fatal_action, 0);
 #ifdef SIGPROF
+#undef SIGPROF
+#endif
+#ifdef SIGPROF
   signal (SIGPROF, SIG_IGN);
 #endif
 #ifdef SIGVTALRM
Index: src/syssignal.h
===================================================================
--- src/syssignal.h.orig
+++ src/syssignal.h
@@ -49,6 +49,10 @@ extern void unblock_tty_out_signal (sigs
 # define HAVE_ITIMERSPEC
 #endif
 
+#ifdef SIGPROF
+#undef SIGPROF
+#endif
+
 #if (defined SIGPROF && !defined PROFILING \
      && (defined HAVE_SETITIMER || defined HAVE_ITIMERSPEC))
 # define PROFILER_CPU_SUPPORT
