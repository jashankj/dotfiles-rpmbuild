Name:		xsecurelock
Version:	1.9.0
Release:	4%{?dist}
Summary:	X11 screen lock utility with security in mind
License:	Apache-2.0

%global	forgeurl	https://github.com/google/xsecurelock
%forgemeta

%bcond_without	mplayer
%bcond_without	mpv
%bcond_without	xscreensaver

URL:		%{forgeurl}
# XXX xsecurelock srcdist is not the same as a `git archive' of the tag.
#Source:	${forgesource}
Source:		%{url}/releases/download/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	libtool
BuildRequires:	gcc
BuildRequires:	make

BuildRequires:	doxygen
BuildRequires:	httpd-tools
BuildRequires:	pam-devel
BuildRequires:	pamtester
BuildRequires:	pandoc
BuildRequires:	pkgconfig(fontconfig)
BuildRequires:	pkgconfig(libbsd)
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(xft)
BuildRequires:	pkgconfig(xcomposite)
BuildRequires:	pkgconfig(xmu)
BuildRequires:	pkgconfig(xrandr)

%if %{with xscreensaver}
BuildRequires:	xscreensaver
%endif

%description
XSecureLock is an X11 screen lock utility designed with the primary goal of
security.

%prep
%forgesetup	-v

%conf 
autoreconf --symlink --install --verbose --force

%configure \
%if %{with mpv}
	--with-mpv=%{_bindir}/mpv \
%endif
%if %{with mplayer}
	--with-mplayer=%{_bindir}/mplayer \
%endif
%if %{with xscreensaver}
	--with-xscreensaver=%{_libexecdir}/xscreensaver \
%endif
	--with-pam-service-name=system-auth \
	--with-xft

%build
%set_build_flags
%make_build

%install
%make_install
rm %{buildroot}%{_pkgdocdir}/LICENSE

%files
%license	LICENSE
%doc	README.md
%doc	CONTRIBUTING
%doc	/usr/share/doc/xsecurelock/examples/saver_livestreams
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_libexecdir}/%{name}/auth_x11
%{_libexecdir}/%{name}/authproto_htpasswd
%{_libexecdir}/%{name}/authproto_pam
%{_libexecdir}/%{name}/authproto_pamtester
%{_libexecdir}/%{name}/dimmer
%{_libexecdir}/%{name}/pgrp_placeholder
%{_libexecdir}/%{name}/saver_blank
%{_libexecdir}/%{name}/saver_multiplex
%{_libexecdir}/%{name}/until_nonidle

%if %{with mpv}
%{_libexecdir}/%{name}/saver_mplayer
%endif

%if %{with mplayer}
%{_libexecdir}/%{name}/saver_mpv
%endif

%if %{with xscreensaver}
%{_libexecdir}/%{name}/saver_xscreensaver
%endif

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 1.9.0-4
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 1.9.0-3
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 1.9.0-2
- Bump for Fedora 40 mass rebuild.

* Sun Dec 24 2023 Jashank Jeremy <fedora@jashankj.space> - 1.9.0-1
- Update to 1.9.0.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 1.8.0-2
- Bump for Fedora 39 mass rebuild; add autoreconf call.

* Tue Dec 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.8.0-1
- Update to 1.8.0.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.7.0-7.1
- Bump for Fedora 37 mass rebuild.

* Mon Apr 25 2022 Jashank Jeremy <fedora@jashankj.space> - 1.7.0-6.1
- Munge support for xscreensaver.

* Sat Jan 22 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.7.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Fri Jul 23 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.7.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Thu Jan 28 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.7.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Mon Sep 14 2020 Sam P <survient@fedoraproject.org> - 1.7.0-3
- Added --with-xft build flag

* Wed Jul 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1.7.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Fri Feb 21 2020 Sam P <survient@fedoraproject.org> - 1.7.0-1
- Initial Build
