%ifnarch %{ocaml_native_compiler}
%global	debug_package	%{nil}
%endif

Name:		ocaml-hmap
Version:	0.8.1
Release:	2%{?dist}
Summary:	Heterogeneous value maps for OCaml
License:	ISC

URL:		https://erratique.ch/software/hmap
Source:		https://erratique.ch/software/hmap/releases/hmap-%{version}.tbz

BuildRequires:	ocaml
BuildRequires:	ocaml-topkg-devel
BuildRequires:	ocaml-findlib-devel
BuildRequires:	ocaml-ocamlbuild
BuildRequires:	ocaml-rpm-macros
BuildRequires:	python3

%description
Hmap provides heterogeneous value maps for OCaml.  These maps bind keys
to values with arbitrary types.  Keys witness the type of the value they
are bound to which allows to add and lookup bindings in a type safe
manner.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description	devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%autosetup -n hmap-%{version}

# # Enable debuginfo if upstream does not
# echo true: debug >> _tags

%build
ocaml pkg/pkg.ml build --tests true

%install
mkdir -p %{buildroot}%{ocamldir}/hmap
cp -p _build/{opam,pkg/META} %{buildroot}%{ocamldir}/hmap
%ifarch %{ocaml_native_compiler}
cp -a _build/src/*.{a,cma,cmi,cmt,cmti,cmx,cmxa,cmxs,mli} \
  %{buildroot}%{ocamldir}/hmap
%else
cp -a _build/src/*.{cma,cmi,cmt,cmti,mli} %{buildroot}%{ocamldir}/hmap
%endif

# This macro requires python3 in the buildroot
%ocaml_files

%check
ocaml pkg/pkg.ml test

%files -f .ofiles
%doc README.md CHANGES.md
%license LICENSE.md

%files devel -f .ofiles-devel

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.8.1-2
- Initial _working_ packaging.

* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.8.1-1
- Initial packaging.
