Name:		snixembed
Version:	0.3.3
Release:	3%{?dist}
Summary:	proxy StatusNotifierItems as XEmbedded systemtray-spec icons
License:	MIT

%global	forgeurl	https://git.sr.ht/~steef/snixembed
%forgemeta

URL:		%{forgeurl}
Source:		%{forgesource}

BuildRequires:	make
BuildRequires:	gcc
BuildRequires:	vala
BuildRequires:	valadoc
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(dbusmenu-gtk3-0.4)

%description
snixembed: proxy StatusNotifierItems as XEmbedded systemtray-spec icons

While many status bars for simple X window managers do not (yet) support
StatusNotifierItem for displaying system tray icons, some software does
not fall back to the widely supported XEmbed-based tray icon protocol.
snixembed acts as a proxy between the new and old.  It does this by
presenting itself as a StatusNotifierHost on the session bus, and using
GTK+3 to maintain corresponding XEmbed tray icons.

%prep
%forgesetup	-v

%conf

%build
%set_build_flags
%make_build

%install
%make_install

%files
%license	LICENSE
%doc		README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.3.3-4
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 0.3.3-3
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 0.3.3-2
- Bump for Fedora 40 mass rebuild.

* Wed Nov 01 2023 Jashank Jeremy <fedora@jashankj.space> - 0.3.3-1
- Initial packaging.
