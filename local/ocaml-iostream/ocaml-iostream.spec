%ifnarch %{ocaml_native_compiler}
%global debug_package %{nil}
%endif

Name:		ocaml-iostream
Version:	0.2
Release:	2%{?dist}
Summary:	Generic I/O streams of bytes for OCaml
License:	MIT

%global	forgeurl0	https://github.com/c-cube/ocaml-iostream
%forgemeta

URL:		%{forgeurl}
Source:		%{forgesource0}

BuildRequires:	ocaml
BuildRequires:	ocaml-dune
BuildRequires:	ocaml-odoc

BuildRequires:	ocaml-ounit-devel
BuildRequires:	ocaml-zip-devel, zlib-devel

%description
This library defines generic I/O streams of bytes.  The streams should
be composable, user-definable, and agnostic to the underlying I/O
mechanism; with OCaml 5 it means that they might be backed by an
effect-based scheduler.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description	devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%forgesetup -v

%build
%dune_build
%dune_build @doc

%install
%dune_install

%check
%dune_check

%files -f .ofiles
%doc README.md

%files devel -f .ofiles-devel

%odoc_package

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.2-2
- Set up a documentation package.

* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.2-1
- Initial packaging.
