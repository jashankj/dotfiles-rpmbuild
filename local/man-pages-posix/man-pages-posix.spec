%global	posix_version		2017
%global	posix_release		a
%global	posix_name		man-pages-posix-%{posix_version}
%global	posix_name_rel		%{posix_name}-%{posix_release}

Name:		man-pages-posix
Version:	%{posix_version}%{posix_release}
Release:	6%{?dist}
Summary:	POSIX manual pages (man0p, man1p, man3p)
License:	GPL+ and GPLv2+ and BSD and MIT and Copyright only and IEEE

URL:		https://www.kernel.org/doc/man-pages/
Source:		https://www.kernel.org/pub/linux/docs/man-pages/man-pages-posix/%{posix_name_rel}.tar.xz

BuildRequires:	make

# `man-pages' previously provided the POSIX manual pages.
Conflicts:	man-pages < 5.13-4

Autoreq:	false
BuildArch:	noarch

%description
POSIX manual pages, prepared by the Linux Documentation Project (LDP).

%prep
%setup -q -n %{posix_name}

%build
# nothing to build

%install
%make_install prefix=/usr DESTDIR=$RPM_BUILD_ROOT
mv POSIX-COPYRIGHT COPYRIGHT
mv %{posix_name_rel}.Announce Announce

%files
%doc README COPYRIGHT Announce
%{_mandir}/man0p/*
%{_mandir}/man1p/*
%{_mandir}/man3p/*

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 6.02-7
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 6.02-6
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 6.02-5
- Bump for Fedora 40 mass rebuild.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 6.02-4
- Bump for Fedora 39 mass rebuild.

* Sat Mar 11 2023 Jashank Jeremy <fedora@jashankj.space> - 6.02-3
- Re-roll package `man-pages-posix', removed at 5.13-4 due to spuriously "disallowed" license.
