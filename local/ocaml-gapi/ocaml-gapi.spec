%ifnarch %{ocaml_native_compiler}
%global debug_package %{nil}
%endif

Name:		ocaml-gapi
Version:	0.4.5
Release:	4%{?dist}
Summary:	A simple OCaml client for Google Services
License:	MIT

%global	forgeurl0	https://github.com/astrada/gapi-ocaml
%global	tag		v%{version}
%forgemeta

URL:		%{forgeurl}
Source:		%{forgesource0}

BuildRequires:	ocaml
BuildRequires:	ocaml-dune
BuildRequires:	ocaml-odoc

BuildRequires:	ocaml-cppo
BuildRequires:	ocaml-cryptokit-devel
BuildRequires:	ocaml-yojson-devel >= 1.6.0
BuildRequires:	ocaml-curl-devel
BuildRequires:	ocaml-ounit-devel
BuildRequires:	ocaml-camlp-streams-devel
BuildRequires:	ocaml-ocamlnet-nethttpd-devel

%description
gapi-ocaml is a simple, unofficial, OCaml client for Google Services.
The library supports ClientLogin, OAuth 1.0a, and OAuth 2.0 authentication.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description	devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%forgesetup -v

%build
%dune_build
%dune_build @doc

%install
%dune_install

%check
%dune_check

%files -f .ofiles
%doc README.md
%license LICENSE

%files devel -f .ofiles-devel

%odoc_package

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.4.5-4
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 0.4.5-3
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 0.4.5-2
- Bump for Fedora 40 mass rebuild.

* Sun Dec 24 2023 Jashank Jeremy <fedora@jashankj.space> - 0.4.5-1
- Update to new upstream release 0.4.5.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 0.4.4-2
- Bump for Fedora 39 mass rebuild.

* Sat Mar 11 2023 Jashank Jeremy <fedora@jashankj.space> - 0.4.4-1
- Update to new upstream release 0.4.4.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 0.4.3.git20220531-1
- Update to upstream git20220531 (and Fedora 37 mass rebuild).

* Fri Jun 24 2022 Jashank Jeremy <fedora@jashankj.space> - 0.4.3.git20220427-2
- Bump to rebuild with OCaml 4.14.0.

* Fri May 06 2022 Jashank Jeremy <fedora@jashankj.space> - 0.4.3.git20220427-1
- Initial RPM release.
