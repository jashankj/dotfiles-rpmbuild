%ifnarch %{ocaml_native_compiler}
%global debug_package %{nil}
%endif

Name:		ocaml-ocamlfuse
Version:	2.7.1.cvs11
Release:	2%{?dist}
Summary:	OCaml bindings for FUSE
License:	MIT

%global	forgeurl	https://github.com/astrada/ocamlfuse
%global	tag		v2.7.1_cvs11
%forgemeta

URL:		%{forgeurl}
Source:		%{forgesource0}

BuildRequires:	ocaml
BuildRequires:	ocaml-dune-devel
BuildRequires:	ocaml-findlib
BuildRequires:	ocaml-odoc

BuildRequires:	pkgconfig(fuse)
BuildRequires:	ocaml-camlidl-devel

%description
This is a binding to FUSE for the OCaml programming language, enabling
you to write multithreaded filesystems in the OCaml language.  It has
been designed with simplicity as a goal.  Efficiency has also been a
separate goal.  The Bigarray library is used for read and writes,
allowing the library to do zero-copy in OCaml land.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name} = %{version}-%{release}
%description	devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%forgesetup -v

%build
%dune_build
%dune_build @doc

%install
%dune_install

%check
%dune_check

%files -f .ofiles
%doc	README.md
%license	LICENSE

%files devel -f .ofiles-devel

%odoc_package

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs11-2
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs11-1
- Update to 2.7.1.cvs11; bump for Fedora 41 mass rebuild.

* Sun Mar 31 2024 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs10-1
- Update to 2.7.1.cvs10.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs8-3
- Bump for Fedora 40 mass rebuild.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs8-2
- Bump for Fedora 39 mass rebuild.

* Sat May 06 2023 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs8-1
- Update to 2.7.1.cvs8.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs7-3
- Bump for Fedora 37 mass rebuild.

* Fri Jun 24 2022 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs7-2
- Bump to rebuild with OCaml 4.14.0.

* Thu May 05 2022 Jashank Jeremy <fedora@jashankj.space> - 2.7.1.cvs7-1
- Initial RPM release.
