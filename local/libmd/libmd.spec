# Derived from openSUSE:Factory/libmd, which is
#   Copyright (c) 2021 SUSE LLC

Name:		libmd
Version:	1.1.0
Release:	4%{?dist}
Summary:	Message digest functions from BSD systems
License:	BSD-2-Clause OR BSD-3-Clause OR ISC OR SUSE-Public-Domain

%define sover	0
Group:		Development/Languages/C and C++
URL:		https://www.hadrons.org/software/libmd/
Source0:	https://archive.hadrons.org/software/libmd/%{name}-%{version}.tar.xz
Source1:	https://archive.hadrons.org/software/libmd/%{name}-%{version}.tar.xz.asc
Source2:	%{name}.keyring
BuildRequires:	pkgconfig
BuildRequires:	gcc

%description
The libmd library provides a few message digest ("hash") functions, as
found on various BSDs on a library with the same name and with a compatible
API.

%if "%{_vendor}" == "suse"
%package	-n %{name}%{sover}
Summary:	Provides message digest functions from BSD systems
Group:		System/Libraries
%description	-n %{name}%{sover}
The libmd library provides a few message digest ("hash") functions, as
found on various BSDs on a library with the same name and with a compatible
API.

Digests supported: MD2/4/5, RIPEMD160, SHA1, SHA2-256/384/512.
%endif

%package	devel
%if "%{_vendor}" == "suse"
Summary:	Development files for%{name}
Group:		Development/Languages/C and C++
Requires:	%{name}%{sover} = %{version}
%description	devel
The libmd library provides a few message digest ("hash") functions, as
found on various BSDs on a library with the same name and with a compatible
API.

Digests supported: MD2/4/5, RIPEMD160, SHA1, SHA2-256/384/512.

%elif "%{_vendor}" == "redhat"
Summary:	Provides message digest functions from BSD systems
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description	devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%endif

%prep
%autosetup

%build
%configure \
	--disable-static \
	--disable-silent-rules

%if 0%{?do_profiling}
	%make_build CFLAGS="%{optflags} %{cflags_profile_generate}"
	%make_build check CFLAGS="%{optflags} %{cflags_profile_generate}"
	%make_build clean
	%make_build CFLAGS="%{optflags} %{cflags_profile_feedback}"
%else
	%make_build
%endif

%install
%make_install
find %{buildroot} -type f -name "*.la" -delete -print

%check
%make_build check

%if "%{_vendor}" == "redhat"
%{?ldconfig_scriptlets}
%elif "%{_vendor}" == "suse"
%post	-n %{name}%{sover}	-p /sbin/ldconfig
%postun	-n %{name}%{sover}	-p /sbin/ldconfig
%endif

%if "%{_vendor}" == "suse"
%files		-n %{name}%{sover}
%elif "%{_vendor}" == "redhat"
%files
%endif
%license	COPYING
%{_libdir}/%{name}.so.%{sover}*

%files		devel
%license	COPYING
%doc		ChangeLog README
%{_includedir}/*
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/libmd.pc
%{_mandir}/man3/*.3*
%{_mandir}/man7/*.7*

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-4
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-3
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-2
- Bump for Fedora 40 mass rebuild.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-1
- Update to 1.1.0; bump for Fedora 39 mass rebuild.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.0.4-2
- Bump for Fedora 37 mass rebuild.

