%global	commit0		0600cca537f8231fa4e62612235966946b24d049
%global commitdate0	20241023
%global shortcommit0	%(c=%{commit0}; echo ${c:0:11})

Name:		noweb
Version:	2.13^%{commitdate0}g%{shortcommit0}
Release:	1%{?dist}
Summary:	WEB-like literate-programming tool
License:	BSD-2-Clause

# We cannot extract debug data.
%global	debug_package		%{nil}

%global	forgeurl0	https://github.com/nrnrnr/noweb
%forgemeta	-a

URL:		https://www.cs.tufts.edu/~nr/noweb
Source0:	%{forgesource0}

BuildRequires:	icont
BuildRequires:	iconx
BuildRequires:	icon-ipl
#BuildRequires:	emacs
BuildRequires:	texlive-base

Requires:	gawk
Requires:	iconx
Requires:	emacs-filesystem >= %{_emacs_version}
Requires:	texlive-base

%description
noweb is designed to meet the needs of literate programmers while
remaining as simple as possible.  Its primary advantages are
simplicity, extensibility, and language-independence.

The noweb manual is only 3 pages; an additional page explains how to
customize its LaTeX output.  noweb works ``out of the box'' with any
programming language, and supports TeX, LaTeX, and HTML back ends.

The primary sacrifice relative to WEB is that code is not
prettyprinted.

%prep
%forgesetup	-z0 -v

%conf
sed -E \
	-e "s#^CC=.*#CC=${CC}#" \
	-e "s#^CFLAGS=.*#CFLAGS=${CFLAGS}#" \
	-e "s@^#?(LIBSRC=icon)@\1@" \
	-e "s@^#?(LIBSRC=awk)@#\1@" \
	-e "s#^BIN=.*#BIN=\${DESTDIR}%{_bindir}#" \
	-e "s#^LIB=.*#LIB=\${DESTDIR}%{_libdir}/noweb#" \
	-e "s#^LIBNAME=.*#LIBNAME=%{_libdir}/noweb#" \
	-e "s#^MAN=.*#MAN=\${DESTDIR}%{_mandir}#" \
	-e "s#^TEXINPUTS=.*#TEXINPUTS=\${DESTDIR}%{_texmf}/tex/plain/misc#" \
	-e "s#^TEXNAME=.*#TEXNAME=%{_texmf}/tex/plain/misc#" \
	-e "s#^ELISP=.*#ELISP=\${DESTDIR}%{_emacs_sitelispdir}#" \
	-e "/-texhash/d" \
	src/Makefile > src/Makefile.tmp && \
mv src/Makefile.tmp src/Makefile

# Fix up Awk paths:
( cd src; sh awkname gawk )

sed -E \
	-e "s#@\\\$\(LIB\)@#@%{_libdir}/noweb@#" \
	src/lib/Makefile > src/lib/Makefile.tmp && \
mv src/lib/Makefile.tmp src/lib/Makefile

%build
%make_build -C src boot
%make_build -C src all

%install
%make_install -C src

%post
%texlive_post

%posttrans
%texlive_posttrans

%postun
%texlive_postun

%files
%license COPYRIGHT LICENSE
%doc README CHANGES src/FAQ
%{_bindir}/cpif
%{_bindir}/htmltoc
%{_bindir}/nodefs
%{_bindir}/noindex
%{_bindir}/noroff
%{_bindir}/noroots
%{_bindir}/notangle
%{_bindir}/nountangle
%{_bindir}/noweave
%{_bindir}/noweb
%{_bindir}/nuweb2noweb
%{_bindir}/sl2h
%{_emacs_sitelispdir}/noweb-mode.el
%{_libdir}/noweb/autodefs.asdl
%{_libdir}/noweb/autodefs.c
%{_libdir}/noweb/autodefs.icon
%{_libdir}/noweb/autodefs.lrtl
%{_libdir}/noweb/autodefs.mmix
%{_libdir}/noweb/autodefs.pascal
%{_libdir}/noweb/autodefs.promela
%{_libdir}/noweb/autodefs.sml
%{_libdir}/noweb/autodefs.tex
%{_libdir}/noweb/autodefs.yacc
%{_libdir}/noweb/btdefn
%{_libdir}/noweb/disambiguate
%{_libdir}/noweb/docs2comments
%{_libdir}/noweb/elide
%{_libdir}/noweb/emptydefn
%{_libdir}/noweb/finduses
%{_libdir}/noweb/h2a
%{_libdir}/noweb/l2h
%{_libdir}/noweb/markup
%{_libdir}/noweb/mnt
%{_libdir}/noweb/noidx
%{_libdir}/noweb/nt
%{_libdir}/noweb/nwmktemp
%{_libdir}/noweb/nwmtime
%{_libdir}/noweb/pipedocs
%{_libdir}/noweb/tmac.w
%{_libdir}/noweb/toascii
%{_libdir}/noweb/tohtml
%{_libdir}/noweb/toroff
%{_libdir}/noweb/totex
%{_libdir}/noweb/unmarkup
%{_libdir}/noweb/xchunks
%{_mandir}/man1/cpif.1.gz
%{_mandir}/man1/htmltoc.1.gz
%{_mandir}/man1/nodefs.1.gz
%{_mandir}/man1/noindex.1.gz
%{_mandir}/man1/noroff.1.gz
%{_mandir}/man1/noroots.1.gz
%{_mandir}/man1/notangle.1.gz
%{_mandir}/man1/nountangle.1.gz
%{_mandir}/man1/noweave.1.gz
%{_mandir}/man1/noweb.1.gz
%{_mandir}/man1/nuweb2noweb.1.gz
%{_mandir}/man1/sl2h.1.gz
%{_mandir}/man7/nowebfilters.7.gz
%{_mandir}/man7/nowebstyle.7.gz
%{_texmf}/tex/plain/misc/noweb.sty
%{_texmf}/tex/plain/misc/nwmac.tex

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 2.13^20241023g0600cca537f-1
- Bump for Fedora 42 mass rebuild.

* Tue Sep 10 2024 Jashank Jeremy <fedora@jashankj.space> - 2.13-4
- Bump for Fedora 41 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 2.13-3
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 2.13-2
- Bump for Fedora 40 mass rebuild.

* Sun Dec 24 2023 Jashank Jeremy <fedora@jashankj.space> - 2.13-1
- Initial packaging of Noweb.
