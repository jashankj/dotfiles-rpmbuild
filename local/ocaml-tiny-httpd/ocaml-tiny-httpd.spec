%ifnarch %{ocaml_native_compiler}
%global	debug_package	%{nil}
%endif

Name:		ocaml-tiny-httpd
Version:	0.17.0
Release:	2%{?dist}
Summary:	Minimal HTTP server in OCaml
License:	MIT

%global	forgeurl0	https://github.com/c-cube/tiny_httpd
%forgemeta

URL:		%{forgeurl}
Source:		%{forgesource0}

BuildRequires:	ocaml
BuildRequires:	ocaml-dune >= 2.9
BuildRequires:	ocaml-odoc

BuildRequires:	ocaml-hmap-devel
BuildRequires:	ocaml-iostream-devel
BuildRequires:	ocaml-logs-devel
BuildRequires:	ocaml-ounit-devel
BuildRequires:	ocaml-ptime-devel
BuildRequires:	ocaml-qcheck-devel
BuildRequires:	ocaml-qtest-devel >= 2.9
BuildRequires:	ocaml-result-devel
BuildRequires:	ocaml-seq-devel
BuildRequires:	ocaml-zip-devel, zlib-devel
BuildRequires:	curl

%description
Minimal HTTP server using good old threads, with stream abstractions,
simple routing, URL encoding/decoding, static asset serving, and
optional compression with camlzip.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}
%description	devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.

%prep
%forgesetup	-v

%build
%dune_build
%dune_build @doc

%install
%dune_install

#check
#dune_check

%files -f .ofiles
%doc README.md CHANGES.md

%files devel -f .ofiles-devel

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.17.0-2
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 0.17.0-1
- Update to upstream 0.17; bump for Fedora 41 mass rebuild.

* Mon Feb 05 2024 Jashank Jeremy <fedora@jashankj.space> - 0.16-2
- Add new dependency 'logs'.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 0.16-1
- Update to upstream 0.16; bump for Fedora 40 mass rebuild.

* Sun Dec 24 2023 Jashank Jeremy <fedora@jashankj.space> - 0.15-1
- Update to upstream 0.15.

* Fri Oct 20 2023 Jashank Jeremy <fedora@jashankj.space> - 0.14-1
- Update to upstream 0.14.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 0.12-7
- Bump for Fedora 39 mass rebuild.

* Tue Dec 27 2022 Jashank Jeremy <fedora@jashankj.space> - 0.12-6
- Bump for ocaml-ptime 1.0.0 -> 1.1.0.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 0.12-5
- Bump for Fedora 37 mass rebuild.

* Fri Jun 24 2022 Jashank Jeremy <fedora@jashankj.space> - 0.12-4
- Bump to rebuild with OCaml 4.14.0.

* Fri May 06 2022 Jashank Jeremy <fedora@jashankj.space> - 0.12-3
- Fix incorrect package name from previous update.

* Thu May 05 2022 Jashank Jeremy <fedora@jashankj.space> - 0.12-2
- Clean up specfile.

* Thu May 05 2022 Jashank Jeremy <fedora@jashankj.space> - 0.12-1
- Initial RPM release.
