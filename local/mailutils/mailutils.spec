# Distantly derived from openSUSE:Factory/mailutils, which is
#   Copyright (c) 2022 SUSE LLC

Name:		mailutils
Version:	3.18
Release:	1%{?dist}
Summary:	GNU Mailutils
License:	GPL-3.0-or-later AND LGPL-3.0-or-later

%define	somajor	9

Group:		Productivity/Networking/Email/Clients
URL:		https://mailutils.org/
Source0:	https://ftpmirror.gnu.org/mailutils/%{name}-%{version}.tar.xz
Source1:	https://ftpmirror.gnu.org/mailutils/%{name}-%{version}.tar.xz.sig
Source2:	%{name}.keyring
Patch10:	patch-catch-guile22

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	bison
BuildRequires:	flex
BuildRequires:	gcc-c++
BuildRequires:	gettext-devel
BuildRequires:	help2man
BuildRequires:	libtool
BuildRequires:	m4
BuildRequires:	pkgconfig
BuildRequires:	texinfo

BuildRequires:	cpio
BuildRequires:	emacs
BuildRequires:	freeradius-client-devel
BuildRequires:	fribidi-devel
BuildRequires:	gdbm-devel
BuildRequires:	gnutls-devel >= 1.0.18
BuildRequires:	guile22-devel
BuildRequires:	hostname
BuildRequires:	libgsasl-devel >= 0.2.3
BuildRequires:	libpq-devel
BuildRequires:	libtool-ltdl-devel
BuildRequires:	libunistring-devel
BuildRequires:	mariadb-connector-c-devel
BuildRequires:	openldap-devel
BuildRequires:	pam-devel
BuildRequires:	pkgconfig(krb5-gssapi)
BuildRequires:	python3-devel
BuildRequires:	readline-devel
#
# I'm not convinced that these BRs are actually correct.
# Does `pkgconfig()' imply the dependency is discovered via pkgconfig?

# mimeview uses /usr/share/cups/mime/mime.types.
Requires:	cups


%description
Mailutils is a set of utilities and daemons for processing e-mail.

All Mailutils programs are able to operate on mailboxes of various
formats, including UNIX maildrops, maildir, and transparently accessed
remote mailboxes (IMAP4, POP3, SMTP).

Included is an implementation of the traditional UNIX mail reader,
"mail", command line utilities such as "frm", "messages", "readmsg", as
well as "sieve", a flexible utility for filtering the incoming mail.

A special feature of Mailutils is an implementation of the MH Message
Handling System, which combines the UNIX philosophy with a flexibility
of Mailutils libraries, thus allowing to incorporate mail from remote
mailboxes.

For system administrators, Mailutils provides a set of daemons for
delivering and reading electronic mail, including pop3d, imap4d and a
universal mail delivery agent, called maidag.


%if "%{_vendor}" == "redhat"
%package	-n libmailutils
%elif "%{_vendor}" == "suse"
%package	-n libmailutils%{somajor}
%endif
Summary:	Generalized mailbox access library
Group:		System/Libraries
%if "%{_vendor}" == "redhat"
%description	-n libmailutils
%elif "%{_vendor}" == "suse"
%description	-n libmailutils%{somajor}
%endif
The runtime library libmailutils contains various mailbox access
routines and support for a number of mailbox types, such as mbox,
Maildir, MH, POP3, and IMAP4. It also supports MIME message
handling, and sending mail via SMTP and Sendmail.


%package	devel
Summary:	Development files for GNU Mailutils
Group:	Development/Libraries/C and C++
%if "%{_vendor}" == "suse"
Requires:	libmailutils%{somajor} = %{version}
Requires:	mailutils = %{version}
%elif "%{_vendor}" == "redhat"
Requires:	libmailutils%{?_isa} = %{version}-%{release}
Requires:	mailutils%{?_isa} = %{version}-%{release}
%endif
%description	devel
This package includes libraries and header files for building tools to
access mailutils features.


%package	mh
Summary:	Mailutils: The GNU Message Handling system
Group:		Productivity/Networking/Email/Clients
%description	mh
The implementation provides an interface between Mailutils and Emacs
using the mh-e module.

To use Mailutils MH with Emacs, add the following line to site-start.el
or .gnu-emacs file: (load "mailutils-mh")


%package	comsatd
Summary:	Mailutils: incoming e-mail notification daemon
Group:		Productivity/Networking/Email/Servers
%description	comsatd
GNU Comsatd is the server which receives reports of incoming mail and
notifies users, wishing to get this service.  It can be started either
from 'inetd.conf' or as a standalone daemon.


%package	delivery
Summary:	Mailutils: delivery agents (maidag, lmtp, putmail)
Group:		Productivity/Networking/Email/Servers
%description delivery
The name 'maidag' stands for Mail Delivery Agent.  It is a general
purpose MDA offering a rich set of features.  It can operate both in
traditional mode, reading the message from its standard input, and in
LMTP mode.  Maidag is able to deliver mail to any mailbox format,
supported by GNU Mailutils.

These formats, among others, include 'smtp://', 'prog://' and
'sendmail://', which are equivalent to forwarding a message over SMTP to
a remote node.


%package	imap4d
Summary:	Mailutils: IMAP4 daemon
Group:		Productivity/Networking/Email/Servers
%description	imap4d
GNU 'imap4d' is a daemon implementing IMAP4 rev1 protocol for accessing
and handling electronic mail messages on a server.


%package	pop3d
Summary:	Mailutils: POP3 daemon
Group:		Productivity/Networking/Email/Servers
%description	pop3d
The 'pop3d' daemon implements the Post Office Protocol Version 3 server.


%package 	guile
Summary:	GNU Mailutils: Guile bindings
%description 	guile
This package contains Guile bindings for GNU Mailutils.

%package	-n python3-%{name}
Summary:	GNU Mailutils: Python bindings
%description	-n python3-%{name}
This package contains Python bindings for GNU Mailutils.


%prep
%setup -q
%patch	10

autoreconf -vfi

%build
%configure \
	--enable-ipv6 \
	--enable-build-servers \
	--enable-build-clients \
	--disable-rpath \
	--disable-static \
	--with-gsasl \
	--with-gdbm \
	--with-mysql \
	--with-postgres \
	--with-gnu-ld \
	--with-gssapi \
	--with-ldap \
	--with-lispdir=%{_datadir}/emacs/site-lisp \
	--with-guile-site-dir=%{_datadir}/guile/site \
	--with-log-facility=LOG_MAIL \
	CFLAGS="$CFLAGS" \
	CXXFLAGS="$CXXFLAGS" \
	DEFAULT_CUPS_CONFDIR=%{_datarootdir}/cups/mime \
	GUILE="$(which guile2.2)" \
	GUILE_CONFIG="$(which guile-config2.2)" \
	GUILE_SNARF="$(which guile-snarf2.2)" \
	GUILE_TOOLS="$(which guile-tools2.2)"
%make_build


%install
%make_install


# Tidy up: remove dir, .la and .elc files
find %{buildroot} \( -name dir -o -name '*.la' -o -name '*.elc' \) -print -delete


# Move system manual pages.
mkdir -p %{buildroot}%{_mandir}/man8
for m in pop3d imap4d
do
	mv %{buildroot}%{_mandir}/man1/${m}.1 \
	   %{buildroot}%{_mandir}/man8/${m}.8
done


# Create missing manual pages
for m in %{buildroot}%{_bindir}/*
do
	case "${m##*/}" in
	guimb|mailutils-config|mu-mh) continue ;;
	esac
	if ! [ -e "%{buildroot}%{_mandir}/man1/${m##*/}.1" ]
	then
		LD_LIBRARY_PATH=%{buildroot}%{_libdir} help2man $m \
			> %{buildroot}%{_mandir}/man1/${m##*/}.1
	fi
done
for m in %{buildroot}%{_sbindir}/*
do
	if ! [ -e "%{buildroot}%{_mandir}/man8/${m##*/}.8" ]
	then
		LD_LIBRARY_PATH=%{buildroot}%{_libdir} help2man $m \
			> %{buildroot}%{_mandir}/man8/${m##*/}.8
	fi
done


# Rename programs to avoid conflicts
mv %{buildroot}%{_bindir}/mail          %{buildroot}%{_bindir}/mu-mail
mv %{buildroot}%{_mandir}/man1/mail.1   %{buildroot}%{_mandir}/man1/mu-mail.1
mv %{buildroot}%{_sbindir}/imap4d       %{buildroot}%{_sbindir}/mu-imap4d
mv %{buildroot}%{_mandir}/man8/imap4d.8 %{buildroot}%{_mandir}/man8/mu-imap4d.8
mv %{buildroot}%{_sbindir}/lmtpd        %{buildroot}%{_sbindir}/mu-lmtpd
mv %{buildroot}%{_mandir}/man8/lmtpd.8  %{buildroot}%{_mandir}/man8/mu-lmtpd.8
mv %{buildroot}%{_sbindir}/mda          %{buildroot}%{_sbindir}/mu-mda
mv %{buildroot}%{_mandir}/man8/mda.8    %{buildroot}%{_mandir}/man8/mu-mda.8
mv %{buildroot}%{_sbindir}/pop3d        %{buildroot}%{_sbindir}/mu-pop3d
mv %{buildroot}%{_mandir}/man8/pop3d.8  %{buildroot}%{_mandir}/man8/mu-pop3d.8


%{?ldconfig_scriptlets}

%find_lang %{name}


%verifyscript
%verify_permissions	%{_bindir}/dotlock
%verify_permissions	%{_sbindir}/maidag


%files
%license	COPYING COPYING.LESSER
%doc		AUTHORS ChangeLog NEWS README* THANKS

%{_bindir}/decodemail
%{_mandir}/man1/decodemail.1*
%attr(02755,root,root) %verify(not mode) %{_bindir}/dotlock
%{_mandir}/man1/dotlock.1*
%{_bindir}/frm
%{_mandir}/man1/frm.1*
%{_bindir}/from
%{_mandir}/man1/from.1*
%{_bindir}/guimb
%{_bindir}/mailutils
%{_mandir}/man1/mailutils.1*
%{_bindir}/messages
%{_mandir}/man1/messages.1*
%{_bindir}/mimeview
%{_mandir}/man1/mimeview.1*
%{_bindir}/movemail
%{_mandir}/man1/movemail.1*
%{_bindir}/mu-mail
%{_mandir}/man1/mu-mail.1*
%{_bindir}/readmsg
%{_mandir}/man1/readmsg.1*
%{_bindir}/sieve
%{_mandir}/man1/sieve.1*
%{_libdir}/mailutils/editheader.so
%{_libdir}/mailutils/list.so
%{_libdir}/mailutils/moderator.so
%{_libdir}/mailutils/numaddr.so
%{_libdir}/mailutils/pipe.so
%{_libdir}/mailutils/spamd.so
%{_libdir}/mailutils/timestamp.so
%{_libdir}/mailutils/vacation.so
%dir %{_libexecdir}/mailutils/
%{_libexecdir}/mailutils/mailutils-acl
%{_libexecdir}/mailutils/mailutils-cflags
%{_libexecdir}/mailutils/mailutils-dbm
%{_libexecdir}/mailutils/mailutils-filter
%{_libexecdir}/mailutils/mailutils-flt2047
%{_libexecdir}/mailutils/mailutils-imap
%{_libexecdir}/mailutils/mailutils-info
%{_libexecdir}/mailutils/mailutils-ldflags
%{_libexecdir}/mailutils/mailutils-logger
%{_libexecdir}/mailutils/mailutils-maildir_fixup
%{_libexecdir}/mailutils/mailutils-pop
%{_libexecdir}/mailutils/mailutils-query
%{_libexecdir}/mailutils/mailutils-send
%{_libexecdir}/mailutils/mailutils-smtp
%{_libexecdir}/mailutils/mailutils-stat
%{_libexecdir}/mailutils/mailutils-wicket

%{_infodir}/mailutils.info*.gz


%if "%{_vendor}" == "redhat"
%files	-f %{name}.lang -n libmailutils
%elif "%{_vendor}" == "suse"
%files	-f %{name}.lang -n libmailutils%{somajor}
%endif
%{_libdir}/libmailutils.so.%{somajor}*
%{_libdir}/libmu*.so.%{somajor}*


%files	devel
%{_bindir}/mailutils-config
%{_libdir}/libmailutils.so
%{_libdir}/libmu*.so
%dir %{_includedir}/mailutils/
%dir %{_includedir}/mailutils/sys/
%{_includedir}/mailutils/*.h
%{_includedir}/mailutils/sys/*.h
%{_datadir}/aclocal/mailutils.m4


%files	mh
%dir %{_bindir}/mu-mh/
%{_bindir}/mu-mh/ali
%{_bindir}/mu-mh/anno
%{_bindir}/mu-mh/burst
%{_bindir}/mu-mh/comp
%{_bindir}/mu-mh/fmtcheck
%{_bindir}/mu-mh/folder
%{_bindir}/mu-mh/folders
%{_bindir}/mu-mh/forw
%{_bindir}/mu-mh/inc
%{_bindir}/mu-mh/install-mh
%{_bindir}/mu-mh/mark
%{_bindir}/mu-mh/mhl
%{_bindir}/mu-mh/mhn
%{_bindir}/mu-mh/mhparam
%{_bindir}/mu-mh/mhpath
%{_bindir}/mu-mh/mhseq
%{_bindir}/mu-mh/msgchk
%{_bindir}/mu-mh/next
%{_bindir}/mu-mh/pick
%{_bindir}/mu-mh/prev
%{_bindir}/mu-mh/prompter
%{_bindir}/mu-mh/refile
%{_bindir}/mu-mh/repl
%{_bindir}/mu-mh/rmf
%{_bindir}/mu-mh/rmm
%{_bindir}/mu-mh/scan
%{_bindir}/mu-mh/send
%{_bindir}/mu-mh/show
%{_bindir}/mu-mh/sortm
%{_bindir}/mu-mh/whatnow
%{_bindir}/mu-mh/whom
%dir %{_datadir}/mailutils/
%dir %{_datadir}/mailutils/mh/
%{_datadir}/mailutils/mh/*
%{_datadir}/emacs/site-lisp/mailutils-mh.el


%files	comsatd
%{_sbindir}/comsatd
%{_mandir}/man8/comsatd.8*

%files	delivery
%{_sbindir}/mu-lmtpd
%{_mandir}/man8/mu-lmtpd.8*
%attr(04755,root,root) %verify(not mode) %{_sbindir}/mu-mda
%{_mandir}/man8/mu-mda.8*
%{_bindir}/putmail
%{_mandir}/man1/putmail.1*

%files	imap4d
%{_sbindir}/mu-imap4d
%{_mandir}/man8/mu-imap4d.8*

%files	pop3d
%{_bindir}/popauth
%{_mandir}/man1/popauth.1*
%{_sbindir}/mu-pop3d
%{_mandir}/man8/mu-pop3d.8*


%files	guile
%defattr(-,root,root,-)
%{_libdir}/libguile-mailutils*.so
%{_datadir}/guile/site/mailutils/*.scm
%{_datadir}/guile/site/mailutils/*.txt

%files	-n python3-%{name}
%dir %{python3_sitelib}/mailutils/
%pycached %{python3_sitelib}/mailutils/__init__.py
%pycached %{python3_sitelib}/mailutils/address.py
%pycached %{python3_sitelib}/mailutils/attribute.py
%pycached %{python3_sitelib}/mailutils/auth.py
%pycached %{python3_sitelib}/mailutils/body.py
%pycached %{python3_sitelib}/mailutils/envelope.py
%pycached %{python3_sitelib}/mailutils/error.py
%pycached %{python3_sitelib}/mailutils/filter.py
%pycached %{python3_sitelib}/mailutils/folder.py
%pycached %{python3_sitelib}/mailutils/header.py
%pycached %{python3_sitelib}/mailutils/mailbox.py
%pycached %{python3_sitelib}/mailutils/mailcap.py
%pycached %{python3_sitelib}/mailutils/mailer.py
%pycached %{python3_sitelib}/mailutils/message.py
%pycached %{python3_sitelib}/mailutils/mime.py
%pycached %{python3_sitelib}/mailutils/nls.py
%pycached %{python3_sitelib}/mailutils/registrar.py
%pycached %{python3_sitelib}/mailutils/secret.py
%pycached %{python3_sitelib}/mailutils/sieve.py
%pycached %{python3_sitelib}/mailutils/stream.py
%pycached %{python3_sitelib}/mailutils/url.py
%pycached %{python3_sitelib}/mailutils/util.py
%dir %{python3_sitearch}/mailutils/
%{python3_sitearch}/mailutils/c_api.so


%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 3.18-1
- Update to 3.18; bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 3.17-2
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 3.17-1
- Update to 3.17; bump for Fedora 40 mass rebuild.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 3.16-1
- Update to 3.16; bump for Fedora 39 mass rebuild.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 3.15-3
- Bump for Fedora 37 mass rebuild.

* Fri Jun 24 2022 Jashank Jeremy <fedora@jashankj.space> - 3.15-2
- Bump to rebuild with Python 3.11.0~b3.

* Thu Apr 21 2022 Jashank Jeremy <fedora@jashankj.space> - 3.15-1
- Initial RPM release.
