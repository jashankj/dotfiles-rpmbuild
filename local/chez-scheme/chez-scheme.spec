%global	forgeurl0	https://github.com/cisco/ChezScheme/
%global	_version	10.1.0

%global	forgeurl3	https://github.com/lz4/lz4/
%global	version3	1.9.4

%global	forgeurl1	https://github.com/nanopass/nanopass-framework-scheme/
%global	version1	1.9.2

%global	forgeurl4	https://github.com/dybvig/stex/
%global	version4	1.2.2

%global	forgeurl2	https://github.com/madler/zlib/
%global	version2	1.3.1

%global	forgeurl5	https://github.com/racket/zuo/
%global	version5	1.9

%global	debug_package	%{nil}

Version:	%{_version}
Release:	2%{?dist}

%forgemeta	-a

Name:		chez-scheme
Summary:	A Scheme implementation
URL:		http://cisco.github.io/ChezScheme
License:	Apache-2.0
Source0:	%{forgesource0}
Source1:	%{forgesource1}
Source2:	%{forgesource2}
Source3:	%{forgesource3}
Source4:	%{forgesource4}
Source5:	%{forgesource5}

BuildRequires:	gcc
BuildRequires:	make
BuildRequires:	ncurses-devel
BuildRequires:	curl
BuildRequires:	ca-certificates
BuildRequires:	libuuid-devel
BuildRequires:	libX11-devel

Requires:	libuuid

Patch0:		install-permissions-strip.patch

%description
Chez Scheme is both a programming language and an implementation
of that language, with supporting tools and documentation.

As a superset of the language described in the Revised^6 Report
on the Algorithmic Language Scheme, Chez Scheme supports all
standard features of Scheme, including first-class procedures,
proper treatment of tail calls, continuations, user-defined
records, libraries, exceptions, and hygienic macro expansion.

Chez Scheme also includes extensive support for interfacing with
C and other languages, support for multiple threads possibly
running on multiple cores, non-blocking I/O, and many other
features.

%prep
%forgesetup	-z5 -v
%forgesetup	-z4 -v
%forgesetup	-z3 -v
%forgesetup	-z2 -v
%forgesetup	-z1 -v
%forgesetup	-z0 -v

rmdir nanopass && mv ../nanopass-framework-scheme-%{version1} nanopass
rmdir zlib && mv ../zlib-%{version2} zlib
rmdir lz4 && mv ../lz4-%{version3} lz4
rmdir stex && mv ../stex-%{version4} stex
rmdir zuo && mv ../zuo-%{version5} zuo

%global	_pkg_extra_cflags	-fPIC
%conf
%{_configure} \
	--installprefix=%{_prefix} \
	--installbin=%{_bindir} \
	--installlib=%{_libdir} \
	--installman=%{_mandir} \
	--temproot=%{buildroot} \
	--threads

%build
%make_build

%install
%make_install

chmod 755 \
	%{buildroot}%{_bindir}/petite \
	%{buildroot}%{_bindir}/scheme \
	%{buildroot}%{_bindir}/scheme-script \
	%{buildroot}%{_libdir}/csv%{_version}/*/main.o \
	%{buildroot}%{_libdir}/csv%{_version}/*/kernel.o \

%files
%{_bindir}/petite
%{_bindir}/scheme
%{_bindir}/scheme-script

%dir	%{_libdir}/csv%{_version}
%{_libdir}/csv%{_version}/*

%{_mandir}/man1/*.1.*

%changelog
* Mon Feb 24 2025 Jashank Jeremy <fedora@jashankj.space> - 10.1.0-2
- Delete stray directory.

* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 10.1.0-1
- Update to 10.1.0; bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 10.0.0-2
- Bump for Fedora 41 mass rebuild.

* Sun Mar 31 2024 Jashank Jeremy <fedora@jashankj.space> - 10.0.0-1
- Update to upstream release 10.0.0.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 9.6.4-2
- Bump for Fedora 40 mass rebuild.

* Fri Oct 20 2023 Jashank Jeremy <fedora@jashankj.space> - 9.6.4-1
- Update to upstream release 9.6.4.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 9.5.8a-2
- Fix the build for weird versions; bump for Fedora 39 mass rebuild.

* Sat May 06 2023 Jashank Jeremy <fedora@jashankj.space> - 9.5.8a-1
- Use 'forge' RPM-spec macros, and fix the build.

* Mon Feb 20 2023 Jashank Jeremy <fedora@jashankj.space> - 9.5.8-2
- Use 'forge' RPM-spec macros, and fix the build.

* Sat Sep  3 2022 Jashank Jeremy <fedora@jashankj.space> - 9.5.8-1
- Update to upstream release 9.5.8.

* Fri Nov 20 2020 Quentin Dufour <quentin@dufour.io> - 9.5.4
- Upgrade sources from 9.5.2 to 9.5.4 (there is no 9.5.3 release on github). Changelog is available here: https://github.com/cisco/ChezScheme/blob/v9.5.4/LOG 

* Wed Jul 01 2020 Quentin Dufour <quentin@dufour.io> - 9.5.2-2
- Compile with thread support (appending --thread to ./configure), thanks Jens-Ulrik Peterson for the notification.

* Mon Jun 15 2020 Quentin Dufour <quentin@dufour.io> - 9.5.2-1
- Upgrade sources from 9.5 to 9.5.2. Changelog is avalaible here: https://github.com/cisco/ChezScheme/blob/v9.5.2/LOG
- Patch makefile to fix binary stripping on fedora rawhide that causes a fatal error

* Mon Jun 11 2018 Quentin Dufour <quentin@dufour.io> - 9.5-2
- Update symlink patch to use a hard link instead as recommended in https://github.com/cisco/ChezScheme/pull/307

* Sat May 19 2018 Quentin Dufour <quentin@dufour.io> - 9.5-1
- Initial packaging of Chez Scheme
