Name:		libstorm
Version:	9.30
Release:	1%{?dist}
Summary:	StormLib is an open-source library for Blizzard MPQ archives
License:	MIT

%global	forgeurl0	https://github.com/ladislav-zezula/StormLib
%forgemeta	-a

URL:		%{forgeurl0}
Source0:	%{forgesource0}
Patch001:	patch-CMakeLists.txt

BuildRequires:	gcc
BuildRequires:	g++
BuildRequires:	cmake
BuildRequires:	pkgconfig(zlib)
BuildRequires:	pkgconfig(bzip2)
BuildRequires:	pkgconfig(libtommath)
BuildRequires:	pkgconfig(libtomcrypt)

%description
StormLib is an open-source library for Blizzard MPQ archives.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description	devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%forgesetup	-z0 -v
%patch	-P 001 -p1 -b .001

%conf
%cmake \
	-D BUILD_SHARED_LIBS=ON \
	-D STORM_USE_BUNDLED_LIBRARIES=OFF \
	-D WITH_LIBTOMCRYPT=ON

%build
%cmake_build

%check
%ctest

%install
%cmake_install

%files
%license	LICENSE
%doc		README.md
%doc		doc/History.txt
%doc		doc/The\ MoPaQ\ File\ Format\ 0.9.txt
%doc		doc/The\ MoPaQ\ File\ Format\ 1.0.txt
%{_libdir}/libstorm.so.9
%{_libdir}/libstorm.so.9.22.0

%files devel
%{_includedir}/StormLib.h
%{_includedir}/StormPort.h
%{_libdir}/libstorm.so
%{_libdir}/cmake/StormLib/StormLibConfig.cmake
%{_libdir}/cmake/StormLib/StormLibConfig-noconfig.cmake

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 9.30-1
- Update to 9.30; bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 9.26-1
- Update to 9.26; bump for Fedora 41 mass rebuild.

* Tue Apr 30 2024 Jashank Jeremy <fedora@jashankj.space> - 9.25-1
- Initial packaging.
