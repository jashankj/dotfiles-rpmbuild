# Derived from openSUSE:Factory/nmtree, which is
#   Copyright (c) 2021 SUSE LLC

%global	gitref	netbsd-resync
Name:		mtree-netbsd
Version:	2021.04.03
Release:	%autorelease
Summary:	Utility for mapping directory hierarchies
License:	BSD-3-Clause
Group:		Productivity/File utilities
URL:		https://github.com/archiecobbs/nmtree
# Source:	https://github.com/archiecobbs/nmtree/archive/%{version}.tar.gz
Source:		mtree-netbsd-2021.04.03.tar.gz

%if "%{_vendor}" == "redhat"
Patch11:	patch-Makefile-docdir
%endif
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	gcc
BuildRequires:	libnbcompat-devel
BuildRequires:	make

Provides:	mtree

%description
The mtree utility compares the file hierarchy rooted in the current
directory against a specification read from the standard input. Messages
are written to the standard output for any files whose characteristics do
not match the specification, or which are missing from either the file
hierarchy or the specification.

This is a port of the NetBSD version of mtree.

%prep
%setup -q

%build
autoreconf -vfi -I .
%configure
%make_build

%install
%make_install

%files
%attr(0755,root,root) %{_bindir}/mtree
%attr(0644,root,root) %{_mandir}/man5/mtree.5.gz
%attr(0644,root,root) %{_mandir}/man8/mtree.8.gz
%defattr(0644,root,root,0755)
%doc %{_docdir}/%{name}

%changelog
