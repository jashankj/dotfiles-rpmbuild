Name:		umpqx
Version:	0.git022111c10a
Release:        2%{?dist}
Summary:	umpqx is a simple command-line MPQ extraction utility
License:	GPL-3.0-only

%global	forgeurl0	https://github.com/uakfdotb/umpqx
%global commit		022111c10ae71ebe4e4d11c92e192e13620c701f
%forgemeta	-a

URL:		%{forgeurl0}
Source0:	%{forgesource0}

BuildRequires:	g++
BuildRequires:	make
BuildRequires:	libstorm-devel

%description
umpqx is a simple command-line MPQ extraction utility

%prep
%forgesetup	-z0 -v

%conf

%build
%make_build umpqx LDLIBS=-lstorm

%install

install -d -m 755 %{buildroot}%{_bindir}
install -m 755 umpqx %{buildroot}%{_bindir}/umpqx

install -d -m 755 %{buildroot}%{_datadir}/umpqx
for f in \
	listfile.txt \
	listfile_fast.txt \
	w3mmd.txt
do
	install -m 644 ${f} %{buildroot}%{_datadir}/umpqx/${f}
done

%files
%license	LICENSE
%doc		README
%{_bindir}/umpqx
%dir	%{_datadir}/umpqx
%{_datadir}/umpqx/listfile.txt
%{_datadir}/umpqx/listfile_fast.txt
%{_datadir}/umpqx/w3mmd.txt

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.git022111c10a-3
- Bump for Fedora 41 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 0.git022111c10a-2
- Bump for Fedora 41 mass rebuild.

* Tue Apr 30 2024 Jashank Jeremy <fedora@jashankj.space> - 0.git022111c10a-1
- Initial packaging.
