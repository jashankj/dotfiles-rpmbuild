Name:		pikchr
Version:	2024.12.08.0
Release:	1%{?dist}
Summary:	A pic(1)-like language for diagrams in technical documentation
License:	0BSD

%{!?tcl_version: %global tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitelib: %global tcl_sitelib %{_datadir}/tcl%{tcl_version}}
%{!?tcl_sitearch: %global tcl_sitearch %{_libdir}/tcl%{tcl_version}}

%global	url		https://pikchr.org/home
%global	commit		28916ba1df976bf9
%forgemeta

URL:		%{url}
Source:		%{url}/tarball/%{commit}/%{name}-%{commit}.tar.gz

BuildRequires:	gcc
BuildRequires:	lemon

%description
Pikchr (pronounced "picture") is a PIC-like markup language for diagrams
in technical documentation.  Pikchr is designed to be embedded in fenced
code blocks of Markdown or similar mechanisms of other documentation
markup languages.


%package	devel
Summary:	Development files for Pikchr
Requires:	%{name} = %{version}-%{release}
%description	devel
This package provides development files for Pikchr.


%package	-n tcl-%{name}
Summary:	Tcl extension for Pikchr
Requires:	%{name} = %{version}-%{release}
Requires:	tcl(abi) = 8.6
BuildRequires:	tcl-devel
%description	-n tcl-%{name}
This pcakge provides a Tcl extension for Pikchr.


%prep
%setup	-q -n %{name}-%{commit}

%build
lemon %{name}.y
cp %{name}.h.in %{name}.h

# Emit command-line executable:
${CC} ${CFLAGS} -o %{name} %{name}.c -DPIKCHR_SHELL -lm

# Build a shared object:
${CC} ${CFLAGS} -shared -fPIC -o lib%{name}.so %{name}.c -lm

# Build a Tcl extension:
mkdir tcl-%{name}
${CC} ${CFLAGS} -shared -fPIC \
	-DPIKCHR_TCL \
	-DPACKAGE_NAME="\"%{name}\"" \
	-DPACKAGE_VERSION="\"%{version}\"" \
	-o tcl-%{name}/lib%{name}.so %{name}.c \
	-lm -ltcl%{tcl_version}
cat <<__EOF__ >> tcl-%{name}/pkgIndex.tcl
package ifneeded %{name} %{version} [list load [file join '\$dir' lib%{name}.so]]
__EOF__


%install
install -d $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}/%{name}

install -d $RPM_BUILD_ROOT%{_libdir}
install -m 755 lib%{name}.so $RPM_BUILD_ROOT%{_libdir}/lib%{name}.so

install -d $RPM_BUILD_ROOT%{_includedir}
install -m 644 %{name}.h $RPM_BUILD_ROOT%{_includedir}/%{name}.h

install -d $RPM_BUILD_ROOT%{tcl_sitearch}/%{name}
install -m 755 tcl-%{name}/lib%{name}.so $RPM_BUILD_ROOT%{tcl_sitearch}/%{name}/lib%{name}.so
install -m 644 tcl-%{name}/pkgIndex.tcl  $RPM_BUILD_ROOT%{tcl_sitearch}/%{name}/pkgIndex.tcl

%files
%doc	README.md homepage.md doc/*.md
%{_bindir}/%{name}
%{_libdir}/lib%{name}.so

%files	devel
%{_includedir}/%{name}.h

%files	-n tcl-%{name}
%{tcl_sitearch}/%{name}/pkgIndex.tcl
%{tcl_sitearch}/%{name}/lib%{name}.so

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 2024.12.08.0-1
- Update to current tip; bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 2024.07.24.0-2
- Update to current tip; bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 2023.12.24.0-1
- Update to current tip; bump for Fedora 40 mass rebuild.

* Fri Oct 20 2023 Jashank Jeremy <fedora@jashankj.space> - 2023.08.30.0-1
- Update to current tip.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 2023.08.09.0-1
- Bump for Fedora 39 mass rebuild.

* Sun Apr 23 2023 Jashank Jeremy <fedora@jashankj.space> - 2023.04.14.0-1
- Initial packaging.
