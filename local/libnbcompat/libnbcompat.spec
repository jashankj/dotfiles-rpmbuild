# Derived from openSUSE:Factory/libnbcompat, which is
#   Copyright (c) 2021 SUSE LLC

Name:		libnbcompat
Version:	1.0.2
Release:	1%{?dist}
Summary:	NetBSD compatibility library
License:	BSD-4-Clause

%define	sover 	0

Group:		Development/Libraries/C and C++
URL:		https://github.com/archiecobbs/%{name}
Source:		https://github.com/archiecobbs/%{name}/archive/%{version}.tar.gz
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	gcc
BuildRequires:	libtool
BuildRequires:	make

%description
%{name} is a NetBSD compatibility library that supplies routines used by
NetBSD bootstrap tools that are missing on other operating systems.

%if "%{_vendor}" == "suse"
%package	-n %{name}%{sover}
Summary:	NetBSD compatibility library
Group:		Development/Libraries/C and C++
%description	-n %{name}%{sover}
This package holds the shared library of %{name}.
%endif

%package	devel
Summary:	Development files for %{name}
Group:		Development/Libraries/C and C++
%if "%{_vendor}" == "suse"
Requires:	%{name}%{sover} = %{version}
%elif "%{_vendor}" == "redhat"
Requires:	%{name}%{?_isa} = %{version}-%{release}
%endif
%description	devel
This package holds the development files for %{name}.

%{name} is a NetBSD compatibility library that supplies routines used by
NetBSD bootstrap tools that are missing on other operating systems.

%prep
%setup -q

%build
autoreconf -vfi -I .
%configure --disable-static LIBDIR='%{_libdir}'
%make_build

%install
%make_install
find %{buildroot} -type f -name "*.la" -delete -print

%if "%{_vendor}" == "suse"
%post	-n %{name}%{sover} -p /sbin/ldconfig
%postun	-n %{name}%{sover} -p /sbin/ldconfig
%files	-n %{name}%{sover}
%elif "%{_vendor}" == "redhat"
%{?ldconfig_scriptlets}
%files
%endif
%{_libdir}/%{name}.so.%{sover}*

%files	devel
%{_includedir}/*
%{_libdir}/*.so

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 1.0.2-1
- Update to 1.0.2; bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 1.0.1-5
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 1.0.1-4
- Bump for Fedora 40 mass rebuild.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 1.0.1-3
- Bump for Fedora 39 mass rebuild.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.0.1-2
- Bump for Fedora 37 mass rebuild.

