Name:		xcape
Version:	1.2
Release:	12%{?dist}
Summary:	Configure modifier keys to act as other keys when pressed and released
License:	GPL-3.0-or-later

%global	forgeurl	https://github.com/alols/xcape
%global	commit		a34d6bae27bbd55506852f5ed3c27045a3c0bd9e
%forgemeta

URL:		%{forgeurl}
Source:		%{forgesource}

BuildRequires:	gcc
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(xproto)
BuildRequires:	pkgconfig(xtst)

%description
%{name} allows you to use a modifier key as another key when pressed and
released on its own.  Note that it is slightly slower than pressing the
original key, because the pressed event does not occur until the key is
released.  The default behaviour is to generate the Escape key when Left
Control is pressed and released on its own.


%prep
%forgesetup -v


%build
%make_build CFLAGS="%{optflags}"


%install
%make_install MANDIR=/share/man/man1


%files
%doc	README.md
%license	LICENSE
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*


%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 1.2-12
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 1.2-11
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 1.2-10
- Bump for Fedora 40 mass rebuild.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 1.2-9
- Bump for Fedora 39 mass rebuild.

* Mon Feb 20 2023 Jashank Jeremy <fedora@jashankj.space> - 1.2-8
- Bump for Fedora 37 mass rebuild.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.2-7.20180301gita34d6ba
- Bump for Fedora 37 mass rebuild.

* Tue Apr 19 2022 Jashank Jeremy <fedora@jashankj.space> - 1.2-6.20180301gita34d6b
- Revive package; update; fix up build-requires, summary, description.

* Sat Jul 27 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.2-5.20180106git6ded5b4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Sun Feb 03 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.2-4.20180106git6ded5b4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.2-3.20180106git6ded5b4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.2-2.20180106git6ded5b4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sat Jan 06 2018 Filip Szymański <fszymanski@fedoraproject.org> - 1.2-1.20180106git6ded5b4
- Initial release

* Tue Jan 03 2017 Dawid Zych <dawid.zych@yandex.com> - 1.2-1
- Initial packaging.
