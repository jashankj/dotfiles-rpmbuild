Name:		google-drive-ocamlfuse
Version:	0.7.32
Release:	3%{?dist}
Summary:	A FUSE filesystem for Google Drive.
License:	MIT

%global	forgeurl	https://github.com/astrada/google-drive-ocamlfuse
%global	tag		v0.7.32
%forgemeta

%undefine _package_note_flags
%ifnarch %{ocaml_native_compiler}
%global debug_package %{nil}
%endif

URL:		%{forgeurl}
Source:		%{forgesource}

BuildRequires:	ocaml >= 4.04.0
BuildRequires:	ocaml-dune-devel
BuildRequires:	ocaml-findlib-devel
BuildRequires:	ocaml-odoc

BuildRequires:	ocaml-camlidl-devel
BuildRequires:	ocaml-cryptokit-devel
BuildRequires:	ocaml-extlib-devel
BuildRequires:	ocaml-gapi-devel >= 0.4.2
BuildRequires:	ocaml-ocamlfuse-devel >= 2.7.1.cvs6
BuildRequires:	ocaml-ounit-devel
BuildRequires:	ocaml-sqlite-devel
BuildRequires:	ocaml-tiny-httpd-devel

# And now all the *undocumented* dependencies...
BuildRequires:	ocaml-biniou-devel
BuildRequires:	ocaml-curl-devel
BuildRequires:	ocaml-easy-format-devel
BuildRequires:	ocaml-seq-devel
BuildRequires:	ocaml-yojson-devel
BuildRequires:	ocaml-zarith-devel
BuildRequires:	fuse-devel


%description
%{name} is a FUSE filesystem for Google Drive, written in OCaml.


%prep
%forgesetup	-v

%build
dune build %{?_smp_mflags} --verbose --profile release
dune build @doc

%install
dune install --destdir=%{buildroot}

# We do not want the dune markers
find _build/default/_doc/_html -name .dune-keep -delete

# We do not want the ml files
find %{buildroot}%{_libdir}/ocaml -name \*.ml -delete

# We install the documentation with the doc macro
rm -fr %{buildroot}%{_prefix}/doc

# This is a leaf package; we don't need dune or opam directives.
rm -f %{buildroot}%{_libdir}/ocaml/google-drive-ocamlfuse/{dune-package,opam}


%check
dune runtest --profile release

%files
%doc	README.md
%license	LICENSE
%{_bindir}/google-drive-ocamlfuse
%{_libdir}/ocaml/google-drive-ocamlfuse/META

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 0.7.32-3
- Bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 0.7.32-2
- Bump for Fedora 41 mass rebuild.

* Sun Mar 31 2024 Jashank Jeremy <fedora@jashankj.space> - 0.7.32-1
- Update to upstream 0.7.32.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 0.7.31-2
- Bump for Fedora 40 mass rebuild.

* Sun Dec 24 2023 Jashank Jeremy <fedora@jashankj.space> - 0.7.31-1
- Update to upstream 0.7.31.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 0.7.30-4
- Bump for Fedora 39 mass rebuild.

* Mon Feb 20 2023 Jashank Jeremy <fedora@jashankj.space> - 0.7.30-3
- Use 'forge' RPM-spec macros.

* Tue Dec 27 2022 Jashank Jeremy <fedora@jashankj.space> - 0.7.30-2
- Bump for ocaml-ptime 1.0.0 -> 1.1.0.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 0.7.30-1
- New version 0.7.30 (and bump for Fedora 37 mass rebuild).

* Fri Jun 24 2022 Jashank Jeremy <fedora@jashankj.space> - 0.7.29.git20220427-2
- Bump to rebuild with OCaml 4.14.0.

* Fri May 06 2022 Jashank Jeremy <fedora@jashankj.space> - 0.7.29.git20220427-1
- Initial RPM release.
