%global	_pkgname	ptime

Name:		ocaml-%{_pkgname}
Version:	1.2.0
Release:	1%{?dist}
Summary:	OCaml: POSIX time for OCaml
License:	ISC

%global	forgeurl	https://github.com/dbuenzli/ptime
%forgemeta

%undefine	_package_note_flags
%ifnarch %{ocaml_native_compiler}
%global	debug_package	%{nil}
%endif

URL:		https://erratique.ch/software/ptime
Source:		%{forgesource}
# or		https://erratique.ch/software/ptime/releases/ptime-1.0.0.tbz

BuildRequires:	ocaml >= 4.08.0
BuildRequires:	ocaml-findlib
BuildRequires:	ocaml-ocamlbuild
BuildRequires:	ocaml-ocamldoc
BuildRequires:	ocaml-topkg-devel >= 1.0.3

%description
Ptime has platform independent POSIX time support in pure OCaml.  It
provides a type to represent a well-defined range of POSIX timestamps
with picosecond precision, conversion with date-time values, conversion
with RFC 3339 timestamps and pretty printing to a human-readable,
locale-independent representation.

The additional Ptime_clock library provides access to a system POSIX
clock and to the system's current time zone offset.

Ptime is not a calendar library.

Ptime has no dependency.  Ptime_clock depends on your system library or
JavaScript runtime system. Ptime and its libraries are distributed under
the ISC license.


%package	devel
Summary:	Development files for %{name}
Requires:	%{name} = %{version}-%{release}
%description	devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%forgesetup

# Topkg does watermark replacements only if run inside a git checkout.  Github
# tarballs do not come with a .git directory.  Therefore, we do the watermark
# replacement manually.
for fil in $(find . -type f); do
  sed -e 's,%%%%NAME%%%%,%{_pkgname},' \
      -e 's,%%%%PKG_HOMEPAGE%%%%,%{url},' \
      -e 's,%%%%VERSION%%%%,v%{version},' \
      -e 's,%%%%VERSION_NUM%%%%,%{version},' \
      -i.orig $fil
  touch -r $fil.orig $fil
  rm $fil.orig
done

%build
# Build the library and the tests
ocaml pkg/pkg.ml build --tests true

# Build the documentation
mkdir html
ocamldoc -html -d html -I +compiler-libs \
	-I _build/src \
	-I _build/src/clock \
	-css-style doc/style.css \
	_build/src/ptime.mli \
	_build/src/clock/ptime_clock.mli


%install
# Install the library
export CAMLLIB=%{buildroot}%{_libdir}/ocaml
mkdir -p ${CAMLLIB}/ptime
mkdir -p ${CAMLLIB}/ptime/clock
mkdir -p ${CAMLLIB}/ptime/clock/os
mkdir -p ${CAMLLIB}/ptime/top
mkdir -p ${CAMLLIB}/stublibs
cp -p _build/{opam,pkg/META} ${CAMLLIB}/%{_pkgname}

%ifarch %{ocaml_native_compiler}
cp -a _build/src/ptime.{cma,cmi,cmt,cmti,mli,a,cmx,cmxa,cmxs} ${CAMLLIB}/ptime
%else
cp -a _build/src/ptime.{cma,cmi,cmt,cmti,mli}                 ${CAMLLIB}/ptime
%endif

cp -a _build/src/clock/ptime_clock.{cmi,cmti,mli}                        ${CAMLLIB}/ptime/clock

cp -a _build/src/clock/dllptime_clock_stubs.so                            ${CAMLLIB}/stublibs
cp -a _build/src/clock/libptime_clock_stubs.a                             ${CAMLLIB}/ptime/clock/os
%ifarch %{ocaml_native_compiler}
cp -a _build/src/clock/ptime_clock.{cma,cmi,cmt,cmti,mli,a,cmx,cmxa,cmxs} ${CAMLLIB}/ptime/clock/os
%else
cp -a _build/src/clock/ptime_clock.{cma,cmi,cmt,cmti,mli}                 ${CAMLLIB}/ptime/clock/os
%endif
cp -a _build/src/clock/runtime.js                                         ${CAMLLIB}/ptime/clock/os

%ifarch %{ocaml_native_compiler}
cp -a _build/src/top/ptime_top.{cma,cmi,cmt,a,cmx,cmxa,cmxs} ${CAMLLIB}/ptime/top
%else
cp -a _build/src/top/ptime_top.{cma,cmi,cmt}                 ${CAMLLIB}/ptime/top
%endif


%check
ocaml pkg/pkg.ml test


%files
%doc		CHANGES.md README.md
%license	LICENSE.md
%{_libdir}/ocaml/ptime/META

%dir	%{_libdir}/ocaml/ptime/
%{_libdir}/ocaml/ptime/*.cma
%{_libdir}/ocaml/ptime/*.cmi
%ifarch %{ocaml_native_compiler}
%{_libdir}/ocaml/ptime/*.cmxs
%endif

%dir	%{_libdir}/ocaml/ptime/clock/
%{_libdir}/ocaml/ptime/clock/*.cmi

%dir	%{_libdir}/ocaml/ptime/clock/os/
%{_libdir}/ocaml/ptime/clock/os/*.cma
%{_libdir}/ocaml/ptime/clock/os/*.cmi
%ifarch %{ocaml_native_compiler}
%{_libdir}/ocaml/ptime/clock/os/*.cmxs
%endif
%{_libdir}/ocaml/ptime/clock/os/runtime.js
%{_libdir}/ocaml/stublibs/dllptime_clock_stubs.so

%dir	%{_libdir}/ocaml/ptime/top/
%{_libdir}/ocaml/ptime/top/*.cma
%{_libdir}/ocaml/ptime/top/*.cmi
%ifarch %{ocaml_native_compiler}
%{_libdir}/ocaml/ptime/top/*.cmxs
%endif


%files	devel
%doc	html/*
%{_libdir}/ocaml/ptime/opam

%ifarch %{ocaml_native_compiler}
%{_libdir}/ocaml/ptime/*.a
%{_libdir}/ocaml/ptime/*.cmx
%{_libdir}/ocaml/ptime/*.cmxa
%endif
%{_libdir}/ocaml/ptime/*.cmt
%{_libdir}/ocaml/ptime/*.cmti
%{_libdir}/ocaml/ptime/*.mli

%{_libdir}/ocaml/ptime/clock/*.cmti
%{_libdir}/ocaml/ptime/clock/*.mli

%ifarch %{ocaml_native_compiler}
%{_libdir}/ocaml/ptime/clock/os/*.a
%{_libdir}/ocaml/ptime/clock/os/*.cmx
%{_libdir}/ocaml/ptime/clock/os/*.cmxa
%endif
%{_libdir}/ocaml/ptime/clock/os/*.cmt
%{_libdir}/ocaml/ptime/clock/os/*.cmti
%{_libdir}/ocaml/ptime/clock/os/*.mli

%ifarch %{ocaml_native_compiler}
%{_libdir}/ocaml/ptime/top/*.a
%{_libdir}/ocaml/ptime/top/*.cmx
%{_libdir}/ocaml/ptime/top/*.cmxa
%endif
%{_libdir}/ocaml/ptime/top/*.cmt


%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 1.2.0-1
- Update to 1.2.0; bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-5
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-4
- Bump for Fedora 40 mass rebuild.

* Fri Oct 20 2023 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-3
- Bump to rebuild with OCaml 5.0.0.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-2
- Fix update breakage; bump for Fedora 39 mass rebuild.

* Tue Dec 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.1.0-1
- Update to 1.1.0.

* Wed Jul 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.0.0-4
- Bump for Fedora 37 mass rebuild.

* Fri Jun 24 2022 Jashank Jeremy <fedora@jashankj.space> - 1.0.0-3
- Bump to rebuild with OCaml 4.14.0.

* Thu May 05 2022 Jashank Jeremy <fedora@jashankj.space> - 1.0.0-2
- Initial RPM release.
