Name:		libbncsutil
Version:	1.4.2
Release:	2%{?dist}
Summary:	Blizzard's Battle.Net Chat Service Utility
License:	LGPL-2.1-or-later

%global	forgeurl0	https://github.com/BNETDocs/bncsutil
%forgemeta	-a

URL:		%{forgeurl0}
Source0:	%{forgesource0}
Patch001:	patch-CMakeLists.txt

BuildRequires:	gcc
BuildRequires:	g++
BuildRequires:	cmake
BuildRequires:	gmp-devel

%description
BNCSUtil is the Battle.Net Chat Service Utility, which aids applications
trying to logon to Classic Battle.net™ using the binary protocol.
Specifically, BNCSUtil has functions that help with the cryptography of
game versions, keys, and passwords.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description	devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%forgesetup	-z0 -v
%patch	-P 001 -p1 -b .001

%conf
%cmake

%build
%cmake_build

%check
%ctest

%install
%cmake_install

%files
%license	COPYING
%doc		README.md
%{_libdir}/libbncsutil.so.1
%{_libdir}/libbncsutil.so.1.4.2

%files devel

%{_includedir}/bncsutil/bncsutil.h
%{_includedir}/bncsutil/bsha1.h
%{_includedir}/bncsutil/buffer.h
%{_includedir}/bncsutil/cdkeydecoder.h
%{_includedir}/bncsutil/checkrevision.h
%{_includedir}/bncsutil/decodekey.h
%{_includedir}/bncsutil/file.h
%{_includedir}/bncsutil/keytables.h
%{_includedir}/bncsutil/libinfo.h
%{_includedir}/bncsutil/ms_stdint.h
%{_includedir}/bncsutil/mutil.h
%{_includedir}/bncsutil/mutil_types.h
%{_includedir}/bncsutil/nls.h
%{_includedir}/bncsutil/oldauth.h
%{_includedir}/bncsutil/pe.h
%{_includedir}/bncsutil/sha1.h
%{_includedir}/bncsutil/stack.h
%{_libdir}/libbncsutil.so

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 1.4.2-2
- Bump for Fedora 42 mass rebuild.

* Tue Apr 30 2024 Jashank Jeremy <fedora@jashankj.space> - 1.4.2-1
- Initial packaging.
