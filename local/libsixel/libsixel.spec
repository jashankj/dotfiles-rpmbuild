Name:		libsixel
Version:	1.10.5
Release:	1%{?dist}
Summary:	Sixel encoder/decoder library
License:	unknown

%global	forgeurl	https://github.com/libsixel/libsixel
%forgemeta

URL:		%{forgeurl}
Source:		%{forgesource}

BuildRequires:	meson
BuildRequires:	pkgconfig
BuildRequires:	gcc

BuildRequires:	python3-devel
BuildRequires:	zlib-devel
BuildRequires:	glibc-devel
BuildRequires:	pkgconfig(gdk-pixbuf-2.0)
BuildRequires:	pkgconfig(gdlib)
BuildRequires:	pkgconfig(libcurl)
BuildRequires:	pkgconfig(libjpeg)
BuildRequires:	pkgconfig(libpng)

%description
An encoder and decoder for DEC SIXEL graphics, and some converter programs.

Sixel, short for "six pixels", is a bitmap graphics format supported by
terminals and printers from DEC.  It consists of a pattern six pixels
high and one wide, resulting in 64 possible patterns.  Each possible
pattern is assigned an ASCII character, making the sixels easy to
transmit on 7-bit serial links.

Sixel was first introduced as a way of sending bitmap graphics to DEC
dot matrix printers like the LA50.  After being put into "sixel mode"
the following data was interpreted to directly control six of the pins
in the nine-pin print head.  A string of sixel characters encodes a
single 6-pixel high row of the image.


%package	devel
Summary:	Development headers for libsixel.
Requires:	libsixel = %{version}
%description devel
Development files for libsixel.


#XXX# Python bindings are broken.
# $package	-n python3-${name}
# Summary:	Python bindings for libsixel.
# Requires:	libsixel = ${version}
# $description	-n python3-${name}
# Python bindings for libsixel.


%prep
%forgesetup	-v

%build
%meson
%meson_build

%check
%meson_test

%install
%meson_install
find %{buildroot} -type f -name "*.la" -delete -print
%{?ldconfig_scriptlets}

%files
%{_bindir}/img2sixel
%{_bindir}/sixel2png
%{_libdir}/libsixel.so.1
%{_libdir}/libsixel.so.1.0.0
%{_mandir}/man1/img2sixel.1*
%{_mandir}/man1/sixel2png.1*
#${_mandir}/man5/sixel.5* (no longer installed?)
%{_datadir}/zsh/site-functions/_img2sixel
%{_datadir}/bash-completion/completions/img2sixel

%files	devel
%{_bindir}/libsixel-config
%{_includedir}/sixel.h
%{_libdir}/libsixel.so
%{_libdir}/pkgconfig/libsixel.pc

# $files	-n python3-${name}
# $dir ${python3_sitelib}/libsixel/
# $pycached ${python3_sitelib}/libsixel/__init__.py
# $pycached ${python3_sitelib}/libsixel/decoder.py
# $pycached ${python3_sitelib}/libsixel/encoder.py

%changelog
* Sat Jan 18 2025 Jashank Jeremy <fedora@jashankj.space> - 1.10.5-1
- Update to 1.10.5; bump for Fedora 42 mass rebuild.

* Sun Jul 28 2024 Jashank Jeremy <fedora@jashankj.space> - 1.10.4.git20221121-4
- Bump for Fedora 41 mass rebuild.

* Sun Feb 04 2024 Jashank Jeremy <fedora@jashankj.space> - 1.10.3.git20221121-3
- Bump for Fedora 40 mass rebuild.

* Sat Aug 12 2023 Jashank Jeremy <fedora@jashankj.space> - 1.10.3.git20221121-2
- Bump for Fedora 39 mass rebuild.

* Tue Dec 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.10.3-2.20230219git490ec15
- Use the 'forge' rpmspec macros.

* Tue Dec 27 2022 Jashank Jeremy <fedora@jashankj.space> - 1.10.3.git20221121-1
- Update to live upstream github.com/libsixel/libsixel.
- Switch to new upstream build system; track HEAD until fixes land.

* Sat Aug 06 2022 Jashank Jeremy <fedora@jashankj.space> - 1.8.6-1
- Import from copr:saahriktu/libsixel; clean up.
